# Card game core

## What is it?

This repository contains source code for a Card game utils. To show audience how cards are generated.

### Random
Card shuffling [algorithms](https://gitlab.com/raspberryapps/cardgame-lib/-/blob/development/random/src/commonMain/kotlin/games/raspberry/card_game/random/utils/random/Shuffling.kt?ref_type=heads)

Implementation of [random](https://gitlab.com/raspberryapps/cardgame-lib/-/blob/development/random/src/commonMain/kotlin/games/raspberry/card_game/random/utils/random/CustomRandom.kt?ref_type=heads) that uses cryptography

Implementation of [random](https://gitlab.com/raspberryapps/cardgame-lib/-/tree/development/random/src/commonMain/kotlin/games/raspberry/card_game/random/data/providers?ref_type=heads) using Random.org