plugins {
    `maven-publish`
    signing
}

publishing {
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/57636849/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Deploy-Token"
                value = App.Publish.gitLabDeployToken
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

signing {
    if (project.hasProperty("signing.gnupg.keyName")) {
        useGpgCmd()
        sign(publishing.publications)
    }
}
