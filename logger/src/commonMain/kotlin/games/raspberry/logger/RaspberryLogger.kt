package games.raspberry.logger

import games.raspberry.logger.output.LoggerOutput
import kotlin.js.JsExport

@JsExport
interface RaspberryLogger : LoggerOutput