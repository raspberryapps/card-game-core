package games.raspberry.logger.output

import kotlin.js.JsExport

@JsExport
interface LoggerOutput {
    fun d(tag: String? = null, message: String, payload: Any? = null)

    fun e(tag: String? = null, message: String? = null, exception: Throwable? = null, payload: Any? = null)

    fun w(tag: String? = null, message: String? = null, exception: Throwable? = null, payload: Any? = null)

    fun v(tag: String? = null, message: String? = null, exception: Throwable? = null, payload: Any? = null)

    fun i(tag: String? = null, message: String, payload: Any? = null)
}