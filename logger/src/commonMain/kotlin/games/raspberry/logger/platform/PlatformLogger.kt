package games.raspberry.logger.platform

expect class PlatformLogger() {
    fun logDebug(tag: String?, msg: String)

    fun logError(tag: String?, msg: String, exception: Throwable?)

    fun logWarning(tag: String?, msg: String, exception: Throwable?)

    fun logVerbose(tag: String?, msg: String, exception: Throwable?)

    fun logInfo(tag: String?, msg: String)
}

interface LoggerSource {
    fun logDebug(tag: String? = null, msg: String)

    fun logError(tag: String? = null, msg: String, exception: Throwable? = null)

    fun logWarning(tag: String? = null, msg: String, exception: Throwable? = null)

    fun logVerbose(tag: String? = null, msg: String, exception: Throwable? = null)

    fun logInfo(tag: String?, msg: String)
}

object Logger {

    var loggerSource: LoggerSource = object : LoggerSource {

        private val platformLogger = PlatformLogger()

        override fun logDebug(tag: String?, msg: String) {
            platformLogger.logDebug(tag, msg)
        }

        override fun logError(tag: String?, msg: String, exception: Throwable?) {
            platformLogger.logError(tag, msg, exception)
        }

        override fun logWarning(tag: String?, msg: String, exception: Throwable?) {
            platformLogger.logWarning(tag, msg, exception)
        }

        override fun logVerbose(tag: String?, msg: String, exception: Throwable?) {
            platformLogger.logVerbose(tag, msg, exception)
        }

        override fun logInfo(tag: String?, msg: String) {
            platformLogger.logInfo(tag, msg)
        }
    }

    var enabled = true

    var tagPrefix: String? = null

    fun d(tag: String? = null, message: String) {
        d(tag, message, Category.NONE)
    }

    fun w(
        tag: String? = null,
        message: String? = null,
        exception: Throwable? = null,
        category: Category = Category.NONE,
    ) {
        if (enabled && category.isEnabled) {
            exception?.printStackTrace()
            loggerSource.logWarning(generateTag(tag), generateMsg(message, exception), exception)
        }
    }

    fun d(tag: String? = null, message: String, category: Category = Category.NONE) {
        if (enabled && category.isEnabled)
            loggerSource.logDebug(generateTag(tag), message)
    }

    fun v(
        tag: String? = null,
        message: String? = null,
        exception: Throwable? = null,
        category: Category = Category.NONE
    ) {
        if (enabled && category.isEnabled)
            loggerSource.logVerbose(generateTag(tag), generateMsg(message, exception), exception)
    }

    fun e(
        tag: String? = null,
        message: String? = null,
        exception: Throwable? = null,
        category: Category = Category.NONE,
    ) {
        if (enabled && category.isEnabled) {
            exception?.printStackTrace()
            loggerSource.logError(generateTag(tag), generateMsg(message, exception), exception)
        }
    }

    fun e(tag: String? = null, exception: Throwable? = null) {
        e(tag, null, exception)
    }

    private fun generateTag(tag: String?): String? =
        if (tagPrefix != null && tag != null) "$tagPrefix:$tag" else tag

    enum class Category(val isEnabled: Boolean) {
        LIFECYCLE(true),
        NONE(true)
    }
}

fun getLogTag(tag: String?): String {
    return if (tag != null) "[$tag]" else ""
}

fun generateMsg(msg: String?, exception: Throwable?): String {
    var logMessage = ""
    if (!msg.isNullOrBlank()) {
        logMessage += msg
    }
    if (exception != null) {
        logMessage += " Exception: ${exception.message.orEmpty()}, stackTrace: ${exception.stackTraceToString()}"
    }
    if (logMessage.isBlank()) {
        logMessage = "Unknown error"
    }
    return logMessage
}