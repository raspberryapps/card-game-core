package games.raspberry.logger

import games.raspberry.logger.output.LoggerOutput

class CombinedRaspberryLogger(
    private val logOutputs: MutableList<LoggerOutput>
) : RaspberryLogger {

    fun addOutput(output: LoggerOutput) {
        logOutputs.add(output)
    }

    override fun d(tag: String?, message: String, payload: Any?) {
        logOutputs.forEach {
            it.d(tag, message, payload)
        }
    }

    override fun e(tag: String?, message: String?, exception: Throwable?, payload: Any?) {
        logOutputs.forEach {
            it.e(tag, message, exception, payload)
        }
    }

    override fun w(tag: String?, message: String?, exception: Throwable?, payload: Any?) {
        logOutputs.forEach {
            it.w(tag, message, exception, payload)
        }
    }

    override fun v(tag: String?, message: String?, exception: Throwable?, payload: Any?) {
        logOutputs.forEach {
            it.v(tag, message, exception, payload)
        }
    }

    override fun i(tag: String?, message: String, payload: Any?) {
        logOutputs.forEach {
            it.i(tag, message, payload)
        }
    }

    companion object {
        fun platform(): CombinedRaspberryLogger = CombinedRaspberryLogger(
            logOutputs = mutableListOf(PlatformRaspberryLogger())
        )

        fun empty(): CombinedRaspberryLogger = CombinedRaspberryLogger(
            logOutputs = mutableListOf()
        )
    }
}