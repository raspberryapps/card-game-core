package games.raspberry.logger

import games.raspberry.logger.platform.generateMsg
import kotlin.js.JsExport

@JsExport
interface LoggerEnabledTrigger : RaspberryLogger {
    var enabled: Boolean
}

@Suppress("unused")
fun createLogMessage(
    tag: String?,
    message: String?,
    exception: Throwable?,
    payload: Any?
): String {
    var text = ""
    if (tag != null) {
        text += "$tag: "
    }
    text += generateMsg(message, exception)
    return text
}