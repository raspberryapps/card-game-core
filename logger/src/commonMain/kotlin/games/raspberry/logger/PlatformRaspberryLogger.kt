package games.raspberry.logger

import games.raspberry.logger.platform.Logger

class PlatformRaspberryLogger(override var enabled: Boolean = true) : RaspberryLogger, LoggerEnabledTrigger {

    override fun d(tag: String?, message: String, payload: Any?) {
        if (!enabled) return
        Logger.d(tag, message)
    }

    override fun e(tag: String?, message: String?, exception: Throwable?, payload: Any?) {
        if (!enabled) return
        Logger.e(tag, message, exception)
    }

    override fun w(tag: String?, message: String?, exception: Throwable?, payload: Any?) {
        if (!enabled) return
        Logger.w(tag, message, exception)
    }

    override fun v(tag: String?, message: String?, exception: Throwable?, payload: Any?) {
        if (!enabled) return
        Logger.v(tag, message, exception)
    }

    override fun i(tag: String?, message: String, payload: Any?) {
        if (!enabled) return
        Logger.v(tag, message)
    }
}