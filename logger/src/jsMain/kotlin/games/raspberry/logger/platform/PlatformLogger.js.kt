package games.raspberry.logger.platform

actual class PlatformLogger actual constructor() {
    actual fun logDebug(tag: String?, msg: String) {
        console.log("D:${getLogTag(tag)}: $msg")
    }

    actual fun logError(tag: String?, msg: String, exception: Throwable?) {
        if (exception != null) {
            console.error("${getLogTag(tag)}: $msg, exception: $exception")
        } else {
            console.error("${getLogTag(tag)}: $msg")
        }
    }

    actual fun logWarning(tag: String?, msg: String, exception: Throwable?) {
        console.warn("${getLogTag(tag)}: $msg ${exception ?: ""}")
    }

    actual fun logVerbose(tag: String?, msg: String, exception: Throwable?) {
        console.log("V:${getLogTag(tag)}: $msg ${exception ?: ""}")
    }

    actual fun logInfo(tag: String?, msg: String) {
        console.info("${getLogTag(tag)}: $msg")
    }
}