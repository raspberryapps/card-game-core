package games.raspberry.logger.platform

actual class PlatformLogger actual constructor() {
    actual fun logDebug(tag: String?, msg: String) {
        println("D:${getLogTag(tag)}: $msg")
    }

    actual fun logError(tag: String?, msg: String, exception: Throwable?) {
        if (exception != null) {
            throw exception
        } else {
            error("${getLogTag(tag)}: $msg")
        }
    }

    actual fun logWarning(tag: String?, msg: String, exception: Throwable?) {
        println("W:${getLogTag(tag)}: $msg ${exception ?: ""}")
    }

    actual fun logVerbose(tag: String?, msg: String, exception: Throwable?) {
        println("V:${getLogTag(tag)}: $msg ${exception ?: ""}")
    }

    actual fun logInfo(tag: String?, msg: String) {
        println("I:${getLogTag(tag)}: $msg")
    }
}