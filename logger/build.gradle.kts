import org.jetbrains.kotlin.gradle.plugin.mpp.apple.XCFramework

plugins {
    alias(libs.plugins.kotlinMultiplatform)
    alias(libs.plugins.kotlinSerialization)
    alias(libs.plugins.kotlinDokka)
    alias(libs.plugins.ktLint)
    id("module.publication")
}

kotlin {
    jvm()
    js(IR) {
        binaries.library()
        nodejs()
        browser()
        generateTypeScriptDefinitions()
    }
    val xcf = XCFramework("logger")
    listOf(
        iosX64(), iosArm64(), iosSimulatorArm64(), macosX64(), macosArm64(),
    ).forEach {
        it.binaries.framework {
            baseName = "logger"
            xcf.add(this)
        }
    }

    sourceSets {
        val commonMain by getting {
        }
        val commonTest by getting {
            dependencies {
                implementation(libs.kotlin.test)
                implementation(libs.kotlinx.coroutines.test)
            }
        }
    }
}

ktlint {
    verbose.set(true)
    outputToConsole.set(true)
}
