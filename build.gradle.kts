version = "0.0.45"
group = "io.raspberryapps.cardgame"

plugins {
    //trick: for the same plugin versions in all sub-modules
    alias(libs.plugins.kotlinMultiplatform).apply(false)
}

subprojects {
    println("The subproject name is $name")
    version = rootProject.version
    group = rootProject.group
}