#!/usr/bin/env bash

chmod +x ./scripts/version/get_version.sh
export JAVA_HOME=$(/usr/libexec/java_home -v 17)

./gradlew clean
./gradlew --stacktrace random:publishJsPackageToNpmjsRegistry