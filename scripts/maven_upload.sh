#!/usr/bin/env bash

# Do we need to upgrade version?
UPGRADE_VERSION=false

while [[ "$#" -gt 0 ]]; do
    case $1 in
        -up|--upgrade-version) UPGRADE_VERSION=true; ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

chmod +x ./scripts/version/get_version.sh
chmod +x ./scripts/version/increment_version.sh
#patch version
echo "Do we need to upgrade version?:${UPGRADE_VERSION}"
if [ $UPGRADE_VERSION == true ]
  then
    echo "Updating version..."
    ./scripts/version/increment_version.sh build.gradle.kts patch
    VERSION=$(./scripts/version/get_version.sh build.gradle.kts)
    git tag $VERSION
  else
    echo "Version wasn't updated"
fi

# get version
echo "Version: $(./scripts/version/get_version.sh build.gradle.kts)"

export JAVA_HOME=$(/usr/libexec/java_home -v 17)

./gradlew logger:publishKotlinMultiplatformPublicationToMavenRepository
./gradlew logger:publishAllPublicationsToMavenRepository

./gradlew random:publishKotlinMultiplatformPublicationToMavenRepository
./gradlew random:publishAllPublicationsToMavenRepository

# generate config file
./gradlew core:generateBuildKonfig

./gradlew core:publishKotlinMultiplatformPublicationToMavenRepository
./gradlew core:publishAllPublicationsToMavenRepository