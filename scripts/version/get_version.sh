#!/bin/bash

# File containing the version
FILE=$1

# Check if file path is provided
if [ -z "$FILE" ]; then
    echo "Please provide the file path as an argument."
    exit 1
fi

# Ensure the file exists
if [ ! -f "$FILE" ]; then
    echo "The file '$FILE' does not exist."
    exit 1
fi

# Read the current version from the file
current_version=$(grep 'version = "' $FILE | cut -d '"' -f 2)

# Check if a version was found
if [ -z "$current_version" ]; then
    echo "Version not found in $FILE."
    exit 1
else
    echo "$current_version"
fi
