#!/bin/bash

# File containing the version
FILE=$1

# Part of the version to increment: major, minor, or patch
INCREMENT_TYPE=${2:-patch}  # Defaults to 'patch' if not specified

# Function to increment version
increment_version() {
    local version=$1
    local IFS=.
    local -a parts=($version)

    case "$INCREMENT_TYPE" in
        major)
            parts[0]=$((parts[0]+1))
            parts[1]=0
            parts[2]=0
            ;;
        minor)
            parts[1]=$((parts[1]+1))
            parts[2]=0
            ;;
        patch)
            parts[2]=$((parts[2]+1))
            ;;
        *)
            echo "Unknown increment type: $INCREMENT_TYPE"
            exit 1
            ;;
    esac

    echo "${parts[0]}.${parts[1]}.${parts[2]}"
}

# Read the current version from the file
current_version=$(grep 'version = "' $FILE | cut -d '"' -f 2)

if [[ -z "$current_version" ]]; then
    echo "Version not found in $FILE."
    exit 1
fi

# Increment the version
new_version=$(increment_version $current_version)

# Use different sed syntax based on OS
if [[ "$OSTYPE" == "darwin"* ]]; then
    # macOS
    sed -i '' -e "s/version = \"$current_version\"/version = \"$new_version\"/g" $FILE
else
    # Linux
    sed -i -e "s/version = \"$current_version\"/version = \"$new_version\"/g" $FILE
fi

echo "Version updated from $current_version to $new_version in $FILE."
