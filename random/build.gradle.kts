import org.jetbrains.kotlin.gradle.plugin.mpp.apple.XCFramework

plugins {
    alias(libs.plugins.kotlinMultiplatform)
    alias(libs.plugins.kotlinSerialization)
    alias(libs.plugins.kotlinDokka)
    alias(libs.plugins.ktLint)
    alias(libs.plugins.npm.publish)
    id("module.publication")
}

kotlin {
    jvm()
    js(IR) {
        binaries.library()
        nodejs()
        browser()
        generateTypeScriptDefinitions()
    }
    val xcf = XCFramework("random")
    listOf(
        iosX64(), iosArm64(), iosSimulatorArm64(), macosX64(), macosArm64(),
    ).forEach {
        it.binaries.framework {
            baseName = "random"
            xcf.add(this)
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                // networking
                implementation(libs.ktor.client.core)
                implementation(libs.ktor.client.logging)
                implementation(libs.ktor.client.content.negotiation)
                implementation(libs.ktor.client.serialization.json)
                implementation(libs.kotlinx.coroutines)
                implementation(libs.kotlinx.atomic)

                // Serialization
                implementation(libs.kotlinx.serialization.json)
                implementation(libs.kotlinx.datetime)

                // random
                implementation(libs.secure.random)

                // utils
                implementation(libs.uuid)

                implementation(project(":logger"))
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(libs.kotlin.test)
                implementation(libs.kotlinx.coroutines.test)
            }
        }
        // jvm
        val jvmMain by getting {
            dependencies {
                implementation(libs.ktor.client.okhttp)
            }
        }

        // js
        val jsMain by getting {
            dependencies {
                implementation(libs.ktor.client.js)
            }
        }

        // native
        val iosX64Main by getting
        val iosArm64Main by getting
        val iosSimulatorArm64Main by getting
        val macosX64Main by getting
        val macosArm64Main by getting
        val nativeMain by creating {
            dependsOn(commonMain)
            iosX64Main.dependsOn(this)
            iosArm64Main.dependsOn(this)
            iosSimulatorArm64Main.dependsOn(this)
            macosX64Main.dependsOn(this)
            macosArm64Main.dependsOn(this)
            dependencies {
                implementation(libs.ktor.client.darwin)
            }
        }
    }
}

ktlint {
    verbose.set(true)
    outputToConsole.set(true)
}

npmPublish {
    access.set(PUBLIC)
    version.set(rootProject.version.toString())

    registries {
        npmjs {
            authToken.set("386e9223-5946-4616-ad28-4c3317efc2ae")
        }
    }
    packages {
        named("js") {
            packageName.set("raspberry_games_random")
        }
    }
}
