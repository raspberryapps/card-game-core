package io.raspberryapps.card_game.random.data.providers

import games.raspberry.card_game.random.data.models.CardDeckRequestUserData
import games.raspberry.card_game.random.data.models.CardDecksRequestUserData
import games.raspberry.card_game.random.data.models.UserInfoData
import games.raspberry.card_game.random.data.providers.RandomOrgApiServiceImpl
import games.raspberry.card_game.random.di.HttpParams
import games.raspberry.card_game.random.di.HttpFactory
import kotlinx.coroutines.test.runTest
import kotlin.test.Test
import kotlin.test.assertEquals


internal class RandomApiServiceImplTest {

    private val factory = HttpFactory
    private val httpParams = HttpParams(
        baseUrl = "https://api.random.org"
    )
    private val randomApiService = RandomOrgApiServiceImpl(
        client = factory.provideHttpClient(
            httpParams
        ),
        // test API key 1,000 requests/day;
        apiKey = "68b344df-e962-4bf7-b77e-eea466b7f90a"
    )

    @Test
    fun callApi_getRandomCardDeck_THEN_successResponse_test() = runTest {
        // GIVEN
        val userData = CardDeckRequestUserData(
            players = listOf(
                UserInfoData(
                    id = "id",
                    name = "name",
                ),
                UserInfoData(
                    id = "id",
                    name = "name",
                ),
            ),
            gameId = "gameId",
        )

        // WHEN
        val result = randomApiService.getRandomCardDeck(
            cardsCount = 32,
            max = 31,
            userData = userData,
        )

        // THEN
        assertEquals(result.cardDecks.first().size, 32)
    }

    @Test
    fun callApi_getRandomCardDecks_THEN_successResponse_test() = runTest {
        // GIVEN
        val userData = CardDecksRequestUserData(
            count = 10,
            id = "test_id"
        )

        // WHEN
        val result = randomApiService.getRandomCardDecks(
            decksCount = 10,
            cardsCount = 32,
            max = 31,
            userData = userData
        )

        // THEN
        assertEquals(result.cardDecks.size, 10)
        assertEquals(result.cardDecks.first().size, 32)
    }
}