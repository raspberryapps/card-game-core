package io.raspberryapps.card_game.random.data.providers

import games.raspberry.card_game.random.data.models.CardDeckRequestUserData
import games.raspberry.card_game.random.data.models.GameCardDeckData
import games.raspberry.card_game.random.data.models.UserInfoData
import games.raspberry.card_game.random.data.providers.PoolParams
import games.raspberry.card_game.random.data.providers.RandomPoolProviderImpl
import games.raspberry.card_game.random.di.RandomOrgApiProvider
import games.raspberry.logger.PlatformRaspberryLogger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.test.runTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue


internal class RandomPoolProviderImplTest {
    private val randomApiService = RandomOrgApiProvider.createApi()
    private val randomPoolProvider = RandomPoolProviderImpl(
        randomOrgApiService = randomApiService,
        logger = PlatformRaspberryLogger(),
        params = PoolParams(poolSize = 10),
        dispatcher = Dispatchers.Unconfined
    )

    @Test
    fun GIVEN_oneGame_THEN_generateCardDeck_test() = runTest {
        // GIVEN
        val userData = CardDeckRequestUserData(
            players = listOf(
                UserInfoData(
                    id = "id",
                    name = "name",
                ),
                UserInfoData(
                    id = "id",
                    name = "name",
                ),
            ),
            gameId = "gameId_1",
        )

        // WHEN
        val result = randomPoolProvider.getCardDeck(
            minRoundsCountInPool = 3,
            userData = userData,
            cardsCount = 32,
            max = 31,
        )

        // THEN
        assertEquals(result.getOrThrow().cards.size, 32)
        val cardDecksValue = randomPoolProvider.getGame("gameId_1")
        assertEquals(cardDecksValue != null, true)
    }

    @Test
    fun GIVEN_pool_THEN_removeGameAndPool_test() = runTest {
        // GIVEN
        val userData = CardDeckRequestUserData(
            players = listOf(
                UserInfoData(
                    id = "id",
                    name = "name",
                ),
                UserInfoData(
                    id = "id",
                    name = "name",
                ),
            ),
            gameId = "gameId_1",
        )

        // WHEN
        val result = randomPoolProvider.getCardDeck(
            minRoundsCountInPool = 1,
            userData = userData,
            cardsCount = 32,
            max = 31,
        )

        // THEN
        val cardDecksValueBeforeFinish = randomPoolProvider.getGame("gameId_1")
        val poolBeforeFinish = randomPoolProvider.getRandomPool(cardDecksValueBeforeFinish?.poolId ?: "")
        assertEquals(cardDecksValueBeforeFinish != null, true)
        assertEquals(poolBeforeFinish != null, true)

        randomPoolProvider.setGameFinished("gameId_1")

        // THEN
        assertEquals(result.getOrThrow().cards.size, 32)
        val cardDecksValueAfterFinish = randomPoolProvider.getGame("gameId_1")
        val poolAfterFinish = randomPoolProvider.getRandomPool(cardDecksValueBeforeFinish?.poolId ?: "")
        assertEquals(cardDecksValueAfterFinish == null, true)
        assertEquals(poolAfterFinish == null, true)
    }


    @Test
    fun GIVEN_3Games_WHEN_enough_decksTHEN_generate_one_pool_test() = runTest {
        // GIVEN
        val userData = List(3) {
            CardDeckRequestUserData(
                players = listOf(
                    UserInfoData(
                        id = "id_1_game_$it",
                        name = "name",
                    ),
                    UserInfoData(
                        id = "id_2_game_$it",
                        name = "name",
                    ),
                ),
                gameId = "gameId_$it",
            )
        }

        // WHEN

        for (data in userData) {
            val result = randomPoolProvider.getCardDeck(
                minRoundsCountInPool = 3,
                userData = data,
                cardsCount = 32,
                max = 31,
            )
            // THEN
            assertEquals(result.getOrThrow().cards.size, 32)
        }

        val poolsCount = randomPoolProvider.getRandomPoolsCount()
        assertEquals(poolsCount, 1)
    }

    @Test
    fun GIVEN_4Games_WHEN_not_enough_decks_THEN_generate_second_pool_test() = runTest {
        // GIVEN
        val userData = List(4) {
            CardDeckRequestUserData(
                players = listOf(
                    UserInfoData(
                        id = "id_1_game_$it",
                        name = "name",
                    ),
                    UserInfoData(
                        id = "id_2_game_$it",
                        name = "name",
                    ),
                ),
                gameId = "gameId_$it",
            )
        }

        // WHEN
        for (data in userData) {
            val result = randomPoolProvider.getCardDeck(
                minRoundsCountInPool = 3,
                userData = data,
                cardsCount = 32,
                max = 31,
            )
            // THEN
            assertEquals(result.getOrThrow().cards.size, 32)
        }

        val poolsCount = randomPoolProvider.getRandomPoolsCount()
        assertEquals(poolsCount, 2)
    }

    @Test
    fun GIVEN_1Games_WHEN_multiple_rounds_THEN_use_same_pool_test() = runTest {
        // GIVEN
        val userData = CardDeckRequestUserData(
            players = listOf(
                UserInfoData(
                    id = "id",
                    name = "name",
                ),
                UserInfoData(
                    id = "id",
                    name = "name",
                ),
            ),
            gameId = "gameId_1",
        )

        // WHEN
        for (data in 0 until 3) {
            val result = randomPoolProvider.getCardDeck(
                minRoundsCountInPool = 3,
                userData = userData,
                cardsCount = 32,
                max = 31,
            )
            // THEN
            assertEquals(result.getOrThrow().cards.size, 32)
        }

        val poolsCount = randomPoolProvider.getRandomPoolsCount()
        assertEquals(poolsCount, 1)
    }

    @Test
    fun GIVEN_3Games_WHEN_multiple_rounds_THEN_use_same_pool_test() = runTest {
        // GIVEN
        val userData = List(3) {
            CardDeckRequestUserData(
                players = listOf(
                    UserInfoData(
                        id = "id_1_game_$it",
                        name = "name",
                    ),
                    UserInfoData(
                        id = "id_2_game_$it",
                        name = "name",
                    ),
                ),
                gameId = "gameId_$it",
            )
        }

        // WHEN
        for (round in 0 until 3) {
            for (data in userData) {
                val result = randomPoolProvider.getCardDeck(
                    minRoundsCountInPool = 3,
                    userData = data,
                    cardsCount = 32,
                    max = 31,
                )
                // THEN
                assertEquals(result.getOrThrow().cards.size, 32)
            }
        }

        val poolsCount = randomPoolProvider.getRandomPoolsCount()
        assertEquals(poolsCount, 1)
    }

    @Test
    fun GIVEN_4Games_WHEN_not_enough_decks_AND_multiple_rounds_THEN_generate_second_pool_test() = runTest {
        // GIVEN
        val userData = List(4) {
            CardDeckRequestUserData(
                players = listOf(
                    UserInfoData(
                        id = "id_1_game_$it",
                        name = "name",
                    ),
                    UserInfoData(
                        id = "id_2_game_$it",
                        name = "name",
                    ),
                ),
                gameId = "gameId_$it",
            )
        }

        // WHEN
        for (round in 0 until 3) {
            for (data in userData) {
                val result = randomPoolProvider.getCardDeck(
                    minRoundsCountInPool = 3,
                    userData = data,
                    cardsCount = 32,
                    max = 31,
                )
                // THEN
                assertEquals(result.getOrThrow().cards.size, 32)
            }
        }

        val poolsCount = randomPoolProvider.getRandomPoolsCount()
        assertEquals(poolsCount, 2)
    }

    @Test
    fun GIVEN_1Games_WHEN_multiple_rounds_AND_more_then_expected_THEN_use_same_pool_test() = runTest {
        // GIVEN
        val userData = CardDeckRequestUserData(
            players = listOf(
                UserInfoData(
                    id = "id",
                    name = "name",
                ),
                UserInfoData(
                    id = "id",
                    name = "name",
                ),
            ),
            gameId = "gameId_1",
        )

        // WHEN
        for (data in 0 until 11) {
            val result = randomPoolProvider.getCardDeck(
                minRoundsCountInPool = 3,
                userData = userData,
                cardsCount = 32,
                max = 31,
            )
            // THEN
            assertEquals(result.getOrThrow().cards.size, 32)
        }

        val poolsCount = randomPoolProvider.getRandomPoolsCount()
        assertEquals(poolsCount, 1)
    }
}