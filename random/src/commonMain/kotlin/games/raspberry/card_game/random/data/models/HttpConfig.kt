package games.raspberry.card_game.random.data.models

import kotlin.js.JsExport
import kotlin.js.JsName


@JsExport
data class HttpConfig(
    @JsName("baseUrl")
    val baseUrl: String,
    @JsName("timeoutMillis")
    val timeoutMillis: Int = 30_000,
)