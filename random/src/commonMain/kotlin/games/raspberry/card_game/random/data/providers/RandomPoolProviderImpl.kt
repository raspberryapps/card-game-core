package games.raspberry.card_game.random.data.providers

import com.benasher44.uuid.uuid4
import games.raspberry.card_game.random.data.exceptions.RandomException
import games.raspberry.card_game.random.data.models.*
import games.raspberry.card_game.random.data.providers.RandomOrgApiServiceImpl.Companion.CARD_DECK_SIZE
import games.raspberry.card_game.random.data.providers.RandomPoolProviderImpl.Companion.CARD_DECK_POOL_SIZE
import games.raspberry.card_game.random.data.providers.RandomPoolProviderImpl.Companion.REQUEST_DURATION_WARN_MILLIS
import games.raspberry.card_game.random.data.providers.RandomPoolProviderImpl.Companion.THRESHOLD_FOR_USED_POOL
import games.raspberry.logger.RaspberryLogger
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.js.JsExport
import kotlin.js.JsName
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds

interface RandomPoolProvider {

    suspend fun getCardDeck(
        minRoundsCountInPool: Int,
        userData: CardDeckRequestUserData,
        min: Int = 0,
        max: Int = CARD_DECK_SIZE - 1,
        cardsCount: Int = CARD_DECK_SIZE,
        signed: Boolean = false,
        requestDurationWarnMillis: Long = REQUEST_DURATION_WARN_MILLIS.toLong(),
    ): Result<GameCardDeckData>

    suspend fun prefetchCardDecks(
        min: Int = 0,
        max: Int = CARD_DECK_SIZE - 1,
        cardsCount: Int = CARD_DECK_SIZE,
        signed: Boolean = false,
        requestDurationWarnMillis: Long = REQUEST_DURATION_WARN_MILLIS.toLong(),
    ): Result<Unit>

    @JsName("getGame")
    fun getGame(gameId: GameIdKey): GameCardDecksRecord?

    @JsName("getRandomPool")
    fun getRandomPool(id: RandomPoolIdKey): RandomPoolValue?

    @JsName("getRandomPoolsCount")
    fun getRandomPoolsCount(): Int

    fun setGameFinished(gameId: GameIdKey)
}

@Suppress("NON_EXPORTABLE_TYPE")
@JsExport
data class PoolParams(
    @JsName("poolSize")
    val poolSize: Int = CARD_DECK_POOL_SIZE,
    @JsName("thresholdForUsedPool")
    // min amount of card deck pool should have to be usable
    val thresholdForUsedPool: Int = THRESHOLD_FOR_USED_POOL,
    @JsName("failedRequestAttempts")
    val failedRequestAttempts: Int = 3,
    @JsName("failedAttemptCooldown")
    val failedAttemptCooldown: Duration = (2 * 60 * 1_000L).milliseconds, // 2 min
)

class RandomPoolProviderImpl(
    private val params: PoolParams,
    private val randomOrgApiService: RandomOrgApiService,
    private val logger: RaspberryLogger,
    private val dispatcher: CoroutineDispatcher = Dispatchers.Default,
    private val idsProvider: () -> String = { uuid4().toString() },
) : RandomPoolProvider {
    private var mutex = Mutex()

    private val randomResultsMap = mutableMapOf<RandomPoolIdKey, RandomPoolValue>()
    private val gameDecksMap = mutableMapOf<GameIdKey, GameCardDecksRecord>()

    private var latestFailedAttemptTime: Instant? = null
    private var failedRequestCount = 0

    override suspend fun getCardDeck(
        minRoundsCountInPool: Int,
        userData: CardDeckRequestUserData,
        min: Int,
        max: Int,
        cardsCount: Int,
        signed: Boolean,
        requestDurationWarnMillis: Long,
    ): Result<GameCardDeckData> = withContext(dispatcher) {
        mutex.withLock {
            val randomException = getExceptionIfCooldown()
            if (randomException != null) {
                return@withLock Result.failure(randomException)
            }
            try {
                val cardDeckData = getOrLoadGameCardDeckData(
                    minRoundsCountInPool = minRoundsCountInPool,
                    userData = userData,
                    min = min,
                    max = max,
                    cardsCount = cardsCount,
                    signed = signed,
                    requestDurationWarnMillis = requestDurationWarnMillis,
                )
                return@withLock Result.success(cardDeckData)
            } catch (error: Throwable) {
                return@withLock Result.failure(mapToRandomExceptionIfError(error))
            }
        }
    }

    override suspend fun prefetchCardDecks(
        min: Int,
        max: Int,
        cardsCount: Int,
        signed: Boolean,
        requestDurationWarnMillis: Long

    ): Result<Unit> = withContext(dispatcher) {
        mutex.withLock {
            val randomException = getExceptionIfCooldown()
            if (randomException != null) {
                return@withLock Result.failure(randomException)
            }
            try {
                val randomPoolValue = requestNewPool(
                    min = min,
                    max = max,
                    cardsCount = cardsCount,
                    signed = signed,
                    requestDurationWarnMillis = requestDurationWarnMillis,
                )
                randomResultsMap[randomPoolValue.randomResult.id] = randomPoolValue
                return@withLock Result.success(Unit)
            } catch (error: Throwable) {
                return@withLock Result.failure(mapToRandomExceptionIfError(error))
            }
        }
    }

    private fun getExceptionIfCooldown(): RandomException? {
        val failedAttemptTime = latestFailedAttemptTime
        val currentTime = Clock.System.now()
        if (failedAttemptTime != null && failedRequestCount >= params.failedRequestAttempts) {
            val allowedRequestTime = failedAttemptTime + params.failedAttemptCooldown
            if (allowedRequestTime > currentTime) {
                val exception = RandomException(
                    message = "getCardDeck: cooldown ends in $allowedRequestTime, " +
                            "failedAttemptTime = $failedAttemptTime, " +
                            "currentTime = $currentTime"
                )
                return exception
            } else {
                latestFailedAttemptTime = null
                failedRequestCount = 0
            }
        }
        return null
    }

    private fun mapToRandomExceptionIfError(error: Throwable): RandomException {
        latestFailedAttemptTime = Clock.System.now()
        failedRequestCount++
        val randomException = RandomException(
            message = "getCardDeck: error message = ${error.message}, " +
                    "cooldown ends in ${requireNotNull(latestFailedAttemptTime) + params.failedAttemptCooldown}, " +
                    "currentTime = $latestFailedAttemptTime",
            cause = error,
        )
        logger.w(
            tag = "RandomPoolProvider",
            exception = randomException
        )
        return randomException
    }

    private suspend fun getOrLoadGameCardDeckData(
        minRoundsCountInPool: Int,
        userData: CardDeckRequestUserData,
        min: Int,
        max: Int,
        cardsCount: Int,
        signed: Boolean,
        requestDurationWarnMillis: Long
    ): GameCardDeckData {
        // prepare random pool
        val randomPoolValue = getPreparedOrRequestPool(
            gameId = userData.gameId,
            min = min,
            max = max,
            cardsCount = cardsCount,
            minRoundsCount = minRoundsCountInPool,
            signed = signed,
            requestDurationWarnMillis = requestDurationWarnMillis,
        )
        randomResultsMap[randomPoolValue.randomResult.id] = randomPoolValue

        // prepare card deck
        val cardDeckData = randomPoolValue.getNextCardDeck(userData.gameId)
        val cardDecksRecord = getPreparedGameCardDecksRecord(
            userData = userData,
            poolId = randomPoolValue.randomResult.id,
            signature = randomPoolValue.randomResult.signature,
            cardDeckData = cardDeckData
        )
        gameDecksMap[userData.gameId] = cardDecksRecord
        return cardDeckData
    }

    override fun getGame(gameId: GameIdKey): GameCardDecksRecord? {
        return gameDecksMap[gameId]
    }

    override fun getRandomPool(id: RandomPoolIdKey): RandomPoolValue? {
        return randomResultsMap[id]
    }

    override fun getRandomPoolsCount(): Int {
        return randomResultsMap.size
    }

    override fun setGameFinished(gameId: GameIdKey) {
        val gameCardDecksRecord = gameDecksMap[gameId]
        if (gameCardDecksRecord != null) {
            val randomPoolValue = randomResultsMap[gameCardDecksRecord.poolId]

            // remove pool if needed
            if (randomPoolValue != null) {
                val newPoolValue = randomPoolValue.copy(
                    usedByGames = randomPoolValue
                        .usedByGames.toMutableMap().apply { remove(gameId) }
                        .toMap()
                )
                if (newPoolValue.usedByGames.isEmpty()) {
                    randomResultsMap.remove(gameCardDecksRecord.poolId)
                } else {
                    randomResultsMap[gameCardDecksRecord.poolId] = newPoolValue
                }
            }
        }
        // remove game
        gameDecksMap.remove(gameId)
    }

    private fun getPreparedGameCardDecksRecord(
        poolId: String,
        signature: String?,
        cardDeckData: GameCardDeckData,
        userData: CardDeckRequestUserData,
    ): GameCardDecksRecord {
        val existingCardDeck = gameDecksMap[userData.gameId]
        return if (existingCardDeck != null) {
            GameCardDecksRecord.nextRound(
                newCardDeckData = cardDeckData,
                gameCardDecksRecord = existingCardDeck,
            )
        } else {
            GameCardDecksRecord.firstRound(
                poolId = poolId,
                newCardDeckData = cardDeckData,
                userData = userData,
                signature = signature
            )
        }
    }

    /**
     * - Gets already used pool for this game (Second and more rounds)
     *  ELSE
     * - Gets pool with enough card deck count (First round)
     *  ELSE
     * - Requests new card pool for this game and future games
     */
    private suspend fun getPreparedOrRequestPool(
        gameId: String,
        min: Int,
        max: Int,
        minRoundsCount: Int,
        cardsCount: Int,
        signed: Boolean,
        requestDurationWarnMillis: Long,
    ): RandomPoolValue {
        val usedResultEntry = randomResultsMap
            .filter { it.value.usedByGames.contains(gameId) }
            .entries.firstOrNull()

        // already used pool for previous rounds
        if (usedResultEntry != null) {
            return usedResultEntry.value.copyAndMoveToNextRound(
                gameId = gameId,
            )
        }

        val availableResultEntry = randomResultsMap
            .filter { it.value.isAvailable(minRoundsCount = minRoundsCount) }
            .entries.firstOrNull()

        // can use existing pool (has enough card deck counts for game)
        if (availableResultEntry != null) {
            return availableResultEntry.value.copyAndAttachNewGame(
                gameId = gameId,
                requestedCardDecks = minRoundsCount,
                thresholdForUsedPool = params.thresholdForUsedPool,
            )
        }

        val cardDecks = requestNewPool(
            min = min,
            max = max,
            cardsCount = cardsCount,
            signed = signed,
            requestDurationWarnMillis = requestDurationWarnMillis,
        )
        return cardDecks.copyAndAttachNewGame(
            gameId = gameId,
            requestedCardDecks = minRoundsCount,
            thresholdForUsedPool = params.thresholdForUsedPool,
        )
    }

    private suspend fun requestNewPool(
        min: Int,
        max: Int,
        cardsCount: Int,
        signed: Boolean,
        requestDurationWarnMillis: Long,
    ): RandomPoolValue {
        return withContext(dispatcher) {
            val requestStartTime = Clock.System.now()
            val result = randomOrgApiService.getRandomCardDecks(
                min = min, max = max,
                decksCount = params.poolSize,
                cardsCount = cardsCount,
                userData = CardDecksRequestUserData(
                    id = idsProvider(),
                    count = params.poolSize
                ),
                signed = signed,
            )
            val requestEndTime = Clock.System.now()
            val requestDuration = requestEndTime - requestStartTime
            if (requestDuration > requestDurationWarnMillis.milliseconds) {
                logger.w(
                    "RandomPoolProvider",
                    message = "Card deck request took longer then expected: " +
                            "actual = $requestDuration, " +
                            "expected = ${requestDurationWarnMillis.milliseconds}"
                )
            }

            val newCardDecksValue = RandomPoolValue(
                randomResult = result,
            )
            newCardDecksValue
        }
    }

    companion object {
        // how many card deck to request each time
        const val CARD_DECK_POOL_SIZE = 100

        // min amount of card deck pool should have to be usable
        const val THRESHOLD_FOR_USED_POOL = 3

        // timeouts
        const val REQUEST_DURATION_WARN_MILLIS = 3_000
        const val TIMEOUT_DURATION_WARN_MILLIS = 10_000
    }
}

typealias RandomPoolIdKey = String

data class RandomPoolValue(
    val randomResult: CardDeckResult,
    // gameId and cardDeckIndex
    val usedByGames: Map<GameIdKey, Int> = emptyMap(),
    val reservedCardDecks: Int = 0,
    val isUsed: Boolean = false,
) {
    private val cardDecksCount: Int get() = randomResult.cardDecks.size

    fun isAvailable(minRoundsCount: Int): Boolean {
        if (isUsed) {
            return false
        }
        val availableCount = cardDecksCount - reservedCardDecks
        return availableCount >= minRoundsCount
    }

    fun copyAndAttachNewGame(
        gameId: GameIdKey,
        requestedCardDecks: Int,
        thresholdForUsedPool: Int,
    ): RandomPoolValue {
        val previousReservedCardDeckIndex = this.reservedCardDecks

        val newNewUsedBy = usedByGames.toMutableMap().apply { put(gameId, previousReservedCardDeckIndex) }.toMap()
        val newReservedCardDecks = previousReservedCardDeckIndex + requestedCardDecks
        return copy(
            usedByGames = newNewUsedBy,
            reservedCardDecks = newReservedCardDecks,
            isUsed = newReservedCardDecks + thresholdForUsedPool > cardDecksCount
        )
    }

    fun copyAndMoveToNextRound(gameId: GameIdKey): RandomPoolValue {
        val currentCardDeckIndex = usedByGames.getValue(gameId)
        // in case we have more rounds than expected
        val newCardDeckIndex = if (currentCardDeckIndex == randomResult.cardDecks.lastIndex) {
            0
        } else {
            currentCardDeckIndex + 1
        }
        val newNewUsedBy = usedByGames.toMutableMap().apply { put(gameId, newCardDeckIndex) }.toMap()
        return copy(
            usedByGames = newNewUsedBy,
        )
    }

    fun getNextCardDeck(gameId: GameIdKey): GameCardDeckData {
        val index = usedByGames.getValue(gameId)
        return GameCardDeckData(
            cardDeckIndex = index,
            cards = randomResult.cardDecks[index]
        )
    }
}

typealias GameIdKey = String

