package games.raspberry.card_game.random.data.models

import kotlin.js.JsExport

@JsExport
data class CardDecksRequestUserData(
    val id: String,
    val count: Int,
)

@JsExport
data class CardDeckRequestUserData(
    val players: List<UserInfoData>,
    val gameId: String,
)

@JsExport
data class UserInfoData(
    val id: String,
    val name: String,
)