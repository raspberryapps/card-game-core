package games.raspberry.card_game.random.utils.random

import games.raspberry.logger.PlatformRaspberryLogger
import games.raspberry.logger.RaspberryLogger
import kotlinx.datetime.Clock
import org.kotlincrypto.SecRandomCopyException
import org.kotlincrypto.SecureRandom
import kotlin.random.Random

interface CustomRandom {
    fun nextInt(from: Int = 0, until: Int): Int

    fun nextLong(from: Long, until: Long): Long

    fun nextBoolean(): Boolean

    /**
     * Randomly shuffles elements in this list in-place using the specified [random] instance as the source of randomness.
     *
     * See: https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#The_modern_algorithm
     */
    @SinceKotlin("1.3")
    fun <T> shuffle(list: MutableList<T>): MutableList<T> {
        for (i in list.lastIndex downTo 1) {
            val j = nextInt(0, i + 1)
            list[j] = list.set(i, list[j])
        }
        return list
    }
}

class DefaultRandom(seed: Long = Clock.System.now().toEpochMilliseconds()) : CustomRandom {
    private val random = Random(seed = seed)
    override fun nextInt(from: Int, until: Int): Int {
        return random.nextInt(from, until)
    }

    override fun nextLong(from: Long, until: Long): Long {
        return random.nextLong(from, until)
    }

    override fun nextBoolean(): Boolean {
        return random.nextBoolean()
    }
}

class SecureRandom(private val logger: RaspberryLogger = PlatformRaspberryLogger()) : CustomRandom {
    private val random = SecureRandom()
    private val fallbackRandom = Random

    /// kotlin.random.Random
    override fun nextInt(from: Int, until: Int): Int {
        checkRangeBounds(from, until)
        val n = until - from
        if (n > 0 || n == Int.MIN_VALUE) {
            val rnd = if (n and -n == n) {
                val bitCount = fastLog2(n)
                nextBits(bitCount)
            } else {
                var v: Int
                do {
                    val bits = nextInt().ushr(1)
                    v = bits % n
                } while (bits - v + (n - 1) < 0)
                v
            }
            return from + rnd
        } else {
            while (true) {
                val rnd = nextInt()
                if (rnd in from until until) return rnd
            }
        }
    }

    override fun nextBoolean(): Boolean {
        return nextBits(1) != 0
    }

    private fun nextInt(): Int {
        return nextBits(32)
    }

    private fun nextFloat(): Float {
        return nextBits(24) / ((1 shl 24).toFloat())
    }

    override fun nextLong(from: Long, until: Long): Long {
        checkRangeBounds(from, until)
        val n = until - from
        if (n > 0) {
            val rnd: Long
            if (n and -n == n) {
                val nLow = n.toInt()
                val nHigh = (n ushr 32).toInt()
                rnd = when {
                    nLow != 0 -> {
                        val bitCount = fastLog2(nLow)
                        // toUInt().toLong()
                        nextBits(bitCount).toLong() and 0xFFFF_FFFF
                    }

                    nHigh == 1 ->
                        // toUInt().toLong()
                        nextInt().toLong() and 0xFFFF_FFFF

                    else -> {
                        val bitCount = fastLog2(nHigh)
                        nextBits(bitCount).toLong().shl(32) + (nextInt().toLong() and 0xFFFF_FFFF)
                    }
                }
            } else {
                var v: Long
                do {
                    val bits = nextLong().ushr(1)
                    v = bits % n
                } while (bits - v + (n - 1) < 0)
                rnd = v
            }
            return from + rnd
        } else {
            while (true) {
                val rnd = nextLong()
                if (rnd in from until until) return rnd
            }
        }
    }

    private fun nextLong(): Long {
        // it's okay that the bottom word remains signed.
        return (nextBits(32).toLong() shl 32) + nextBits(32)
    }

    private fun nextBits(numBits: Int): Int {
        val numBytes = (numBits + 7) / 8
        val b = ByteArray(numBytes)
        var next = 0

        nextBytesCopyTo(b)
        for (i in 0 until numBytes) {
            next = (next shl 8) + (b[i].toInt() and 0xFF)
        }

        return next ushr (numBytes * 8 - numBits)
    }

    private fun nextBytesCopyTo(bytes: ByteArray) {
        try {
            random.nextBytesCopyTo(bytes)
        } catch (error: SecRandomCopyException) {
            logger.e("SecureGameRandom", "nextBytesCopyTo error $error")
            fallbackRandom.nextBytes(bytes)
            return
        }
    }

    private fun fastLog2(value: Int): Int = 31 - value.countLeadingZeroBits()

    private fun checkRangeBounds(from: Int, until: Int) = require(until > from) { boundsErrorMessage(from, until) }
    private fun checkRangeBounds(from: Long, until: Long) = require(until > from) { boundsErrorMessage(from, until) }
    private fun checkRangeBounds(from: Double, until: Double) =
        require(until > from) { boundsErrorMessage(from, until) }

    private fun boundsErrorMessage(from: Any, until: Any) = "Random range is empty: [$from, $until)."
}