package games.raspberry.card_game.random.utils.random

/**
 * One iteration of the riffle shuffle is defined as:
 *
 * Split the deck into two piles
 * Merge the two piles by taking one card from the top of either pile in proportion to the number of cards remaining in the pile. To start with the probability for both piles will be 26/52 (50-50), then 25/51-26/51 etc etc as the riffle progresses.
 * The merged deck is now the new "shuffled" deck
 *
 * https://www.youtube.com/watch?v=O9YcMYdTkPY
 * https://rosettacode.org/wiki/Card_shuffles#Kotlin
 */
fun <T> List<T>.riffle(iterations: Int, random: CustomRandom = SecureRandom()): List<T> {
    val deck = this
    val pile = deck.toMutableList()

    repeat(iterations) {
        val mid = deck.size / 2
        val tenpc = mid / 10
        // choose a random number within 10% of midpoint
        val cut = mid - tenpc + random.nextInt(until = 2 * tenpc + 1)
        // split deck into two at cut point
        val deck1 = pile.take(cut).toMutableList()
        val deck2 = pile.drop(cut).toMutableList()
        pile.clear()
        val fromTop = random.nextBoolean() // choose to draw from top or bottom
        while (deck1.size > 0 && deck2.size > 0) {
            if (fromTop) {
                pile.add(deck1.removeAt(0))
                pile.add(deck2.removeAt(0))
            } else {
                pile.add(deck1.removeAt(deck1.lastIndex))
                pile.add(deck2.removeAt(deck2.lastIndex))
            }
        }
        // add any remaining cards to the pile and reverse it
        if (deck1.size > 0) pile.addAll(deck1)
        else if (deck2.size > 0) pile.addAll(deck2)
        pile.reverse() // as pile is upside down
    }
    return pile
}

/**
 * One iteration of the overhand shuffle is defined as:
 *
 * Take a group of consecutive cards from the top of the deck. For our purposes up to 20% of the deck seems like a good amount.
 * Place that group on top of a second pile
 * Repeat these steps until there are no cards remaining in the original deck
 * The second pile is now the new "shuffled" deck
 *
 * https://www.youtube.com/watch?v=A0J_487VquE&ab_channel=52Kards
 */
fun <T> List<T>.overhand(iterations: Int, random: CustomRandom = SecureRandom()): List<T> {
    val deck = this
    val pile = deck.toMutableList()
    val pile2 = mutableListOf<T>()
    val twentypc = deck.size / 5
    repeat(iterations) {
        while (pile.size > 0) {
            val cards = minOf(pile.size, 1 + random.nextInt(until = twentypc))
            pile2.addAll(0, pile.take(cards))
            repeat(cards) { pile.removeAt(0) }
        }
        pile.addAll(pile2)
        pile2.clear()
    }
    return pile
}