package games.raspberry.card_game.random.data.providers

import games.raspberry.card_game.random.data.models.CardDeckResult
import games.raspberry.card_game.random.data.models.CardDeckRequestUserData
import games.raspberry.card_game.random.data.models.CardDecksRequestUserData
import games.raspberry.card_game.random.data.providers.RandomOrgApiServiceImpl.Companion.CARD_DECK_SIZE

interface RandomOrgApiService {
    suspend fun getRandomCardDeck(
        min: Int = 0,
        max: Int = CARD_DECK_SIZE - 1,
        cardsCount: Int = CARD_DECK_SIZE,
        userData: CardDeckRequestUserData,
        signed: Boolean = false,
    ): CardDeckResult

    suspend fun getRandomCardDecks(
        min: Int = 0,
        max: Int = CARD_DECK_SIZE - 1,
        decksCount: Int,
        cardsCount: Int = CARD_DECK_SIZE,
        userData: CardDecksRequestUserData,
        signed: Boolean = false,
    ): CardDeckResult
}