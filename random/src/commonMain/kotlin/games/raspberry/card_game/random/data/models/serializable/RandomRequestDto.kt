package games.raspberry.card_game.random.data.models.serializable

import kotlinx.serialization.Serializable

@Serializable
internal data class RandomRequestDto<T : RandomParamsDto>(
    val jsonrpc: String,
    val method: String,
    val params: T,
    val id: String,
)

internal sealed interface RandomParamsDto {
    @Serializable
    data class RandomIntegersParamsDto(
        val apiKey: String,
        val n: Int,
        val min: Int,
        val max: Int,
        val replacement: Boolean,
        val ticketId: String?,
        val userData: CardDeckUserDataDto?,
    ) : RandomParamsDto

    @Serializable
    data class RandomIntegerSequencesParamsDto(
        val apiKey: String,
        val n: Int,
        val length: List<Int>,
        val min: Int,
        val max: Int,
        val replacement: Boolean,
        val ticketId: String?,
        val userData: CardDecksUserDataDto?,
    ) : RandomParamsDto
}

@Serializable
internal data class CardDeckUserDataDto(
    val players: List<UserInfoDataDto>,
    val gameId: String,
)

@Serializable
internal data class CardDecksUserDataDto(
    val id: String,
    val count: Int,
)

@Serializable
internal data class UserInfoDataDto(
    val id: String,
    val name: String,
)