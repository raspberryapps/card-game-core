package games.raspberry.card_game.random.data.models

data class CardDeckResult(
    val cardDecks: List<List<Int>>,
    val responseText: String,
    val id: String,
    val signature: String?,
)