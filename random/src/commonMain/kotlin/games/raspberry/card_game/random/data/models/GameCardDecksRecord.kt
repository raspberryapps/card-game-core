package games.raspberry.card_game.random.data.models

import kotlin.js.JsExport

@JsExport
data class GameCardDecksRecord(
    val poolId: String,
    val signature: String?,
    val userData: CardDeckRequestUserData,
    val rounds: List<GameCardDeckData> = emptyList(),
) {

    companion object {
        fun firstRound(
            poolId: String,
            signature: String?,
            newCardDeckData: GameCardDeckData,
            userData: CardDeckRequestUserData,
        ): GameCardDecksRecord {
            return GameCardDecksRecord(
                poolId = poolId,
                signature = signature,
                userData = userData,
                rounds = listOf(
                    newCardDeckData
                )
            )
        }

        fun nextRound(
            gameCardDecksRecord: GameCardDecksRecord,
            newCardDeckData: GameCardDeckData,
        ): GameCardDecksRecord {
            return gameCardDecksRecord.copy(
                rounds = gameCardDecksRecord.rounds + newCardDeckData
            )
        }
    }
}

@JsExport
data class GameCardDeckData(
    val cardDeckIndex: Int,
    val cards: List<Int>,
)