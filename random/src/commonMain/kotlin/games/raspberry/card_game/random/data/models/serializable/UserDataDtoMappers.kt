package games.raspberry.card_game.random.data.models.serializable

import games.raspberry.card_game.random.data.models.CardDeckRequestUserData
import games.raspberry.card_game.random.data.models.CardDecksRequestUserData
import games.raspberry.card_game.random.data.models.UserInfoData


internal fun CardDeckUserDataDto.mapFromDto(): CardDeckRequestUserData {
    return CardDeckRequestUserData(
        players = players.map { it.mapFromDto() },
        gameId = gameId,
    )
}

internal fun CardDeckRequestUserData.mapToDto(): CardDeckUserDataDto {
    return CardDeckUserDataDto(
        players = players.map { it.mapToDto() },
        gameId = gameId,
    )
}

internal fun CardDecksUserDataDto.mapFromDto(): CardDecksRequestUserData {
    return CardDecksRequestUserData(
        id = id,
        count = count,
    )
}

internal fun CardDecksRequestUserData.mapToDto(): CardDecksUserDataDto {
    return CardDecksUserDataDto(
        id = id,
        count = count,
    )
}

internal fun UserInfoDataDto.mapFromDto(): UserInfoData {
    return UserInfoData(
        id = id,
        name = name,
    )
}

internal fun UserInfoData.mapToDto(): UserInfoDataDto {
    return UserInfoDataDto(
        id = id,
        name = name,
    )
}