package games.raspberry.card_game.random.data.providers

import games.raspberry.card_game.random.data.exceptions.RandomException
import games.raspberry.card_game.random.data.exceptions.RandomException.Companion.REQUEST_FAIL_CODE
import games.raspberry.card_game.random.data.models.CardDeckRequestUserData
import games.raspberry.card_game.random.data.models.CardDeckResult
import games.raspberry.card_game.random.data.models.CardDecksRequestUserData
import games.raspberry.card_game.random.data.models.serializable.*
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.jsonObject

class RandomOrgApiServiceImpl(
    private val client: HttpClient,
    private val apiKey: String,
) : RandomOrgApiService {

    /**
     * https://api.random.org/json-rpc/4/basic
     * https://api.random.org/json-rpc/4/signed
     */
    override suspend fun getRandomCardDeck(
        min: Int,
        max: Int,
        cardsCount: Int,
        userData: CardDeckRequestUserData,
        signed: Boolean,
    ): CardDeckResult {
        try {
            val response = client.post {
                url("/json-rpc/4/invoke")
                setBody(
                    RandomRequestDto(
                        jsonrpc = "2.0",
                        id = "test_id",
                        method = if (signed) "generateSignedIntegers" else "generateIntegers",
                        params = RandomParamsDto.RandomIntegersParamsDto(
                            apiKey = apiKey,
                            n = cardsCount,
                            min = min,
                            max = max,
                            ticketId = null,
                            userData = if (signed) userData.mapToDto() else null,
                            replacement = false
                        ),
                    )
                )
            }
            val bodyAsJson = response.body<JsonObject>()
            throwIfError(bodyAsJson, response)
            val responseDto = response.body<RandomResponseDto<RandomResultDto.IntegersResultDto>>()
            val randomJson = extractRandomResult(bodyAsJson)
            return CardDeckResult(
                cardDecks = listOf(responseDto.result.random.data),
                signature = responseDto.result.signature,
                responseText = randomJson,
                id = responseDto.id,
            )
        } catch (error: Throwable) {
            when (error) {
                is RandomException -> throw error

                else -> throw RandomException(
                    code = REQUEST_FAIL_CODE,
                    message = "[getRandomCardDeck] error",
                    cause = error
                )
            }
        }
    }

    /**
     * https://api.random.org/json-rpc/4/basic
     * https://api.random.org/json-rpc/4/signed
     */
    override suspend fun getRandomCardDecks(
        min: Int,
        max: Int,
        decksCount: Int,
        cardsCount: Int,
        userData: CardDecksRequestUserData,
        signed: Boolean,
    ): CardDeckResult {
        try {
            val response = client.post {
                url("/json-rpc/4/invoke")
                setBody(
                    RandomRequestDto(
                        jsonrpc = "2.0",
                        id = userData.id,
                        method = if (signed) "generateSignedIntegerSequences" else "generateIntegerSequences",
                        params = RandomParamsDto.RandomIntegerSequencesParamsDto(
                            apiKey = apiKey,
                            n = decksCount,
                            min = min,
                            max = max,
                            length = List(decksCount) {
                                cardsCount
                            },
                            ticketId = null,
                            userData = if (signed) userData.mapToDto() else null,
                            replacement = false
                        ),
                    )
                )
            }
            val bodyAsJson = response.body<JsonObject>()
            throwIfError(bodyAsJson, response)
            val responseDto = response.body<RandomResponseDto<RandomResultDto.IntegerSequencesResultDto>>()
            val randomJson = extractRandomResult(bodyAsJson)
            return CardDeckResult(
                cardDecks = responseDto.result.random.data,
                signature = responseDto.result.signature,
                responseText = randomJson,
                id = responseDto.id,
            )
        } catch (error: Throwable) {
            when (error) {
                is RandomException -> throw error

                else -> throw RandomException(
                    code = REQUEST_FAIL_CODE,
                    message = "[getRandomCardDeck] error, message = ${error.message}",
                    cause = error
                )
            }
        }
    }

    private suspend fun throwIfError(bodyAsJson: JsonObject, response: HttpResponse) {
        if (bodyAsJson.contains("error")) {
            val errorResponseDto = response.body<ErrorResponseDto>()
            throw RandomException(
                code = errorResponseDto.error?.code,
                message = errorResponseDto.error?.message,
            )
        }
    }

    private fun extractRandomResult(bodyAsJson: JsonObject): String {
        return bodyAsJson.getValue("result").jsonObject.getValue("random").toString()
    }

    companion object {
        const val CARD_DECK_SIZE = 52
    }
}