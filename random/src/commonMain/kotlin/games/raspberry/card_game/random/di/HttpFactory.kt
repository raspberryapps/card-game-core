package games.raspberry.card_game.random.di

import games.raspberry.card_game.random.data.models.RandomOrgConfig
import games.raspberry.card_game.random.data.models.mapToParams
import games.raspberry.card_game.random.data.providers.RandomOrgApiService
import games.raspberry.card_game.random.data.providers.RandomOrgApiServiceImpl
import games.raspberry.card_game.random.di.HttpFactory.provideHttpClient
import io.ktor.client.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.request.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json

expect object HttpFactory {

    fun provideHttpClient(
        config: HttpParams,
        externalLogger: (message: String) -> Unit = { message -> games.raspberry.logger.platform.Logger.d("[RandomOrgAPI]", message) }
    ): HttpClient
}

object RandomOrgApiProvider {

    // test API key 1,000 requests/day;
    const val TEST_RANDOM_ORG_API_KEY = "68b344df-e962-4bf7-b77e-eea466b7f90a"
    const val RANDOM_ORG_API_URL = "https://api.random.org"

    fun createApi(
        randomOrgConfig: RandomOrgConfig = RandomOrgConfig()
    ): RandomOrgApiService {
        return RandomOrgApiServiceImpl(
            client = provideHttpClient(randomOrgConfig.httpConfig.mapToParams()),
            apiKey = randomOrgConfig.apiKey
        )
    }
}

object ApiCommonConfigs {

    fun HttpClientConfig<*>.setupJsonCommon(
        config: HttpParams,
        externalLogger: (message: String) -> Unit
    ) {
        expectSuccess = true
        setupJsonDefaultRequest(config.baseUrl)
        setupContentNegotiation(config.json)
        setupLogging(config, externalLogger)
    }

    fun HttpClientConfig<*>.setupJsonDefaultRequest(url: String) {
        defaultRequest {
            header("Content-Type", "application/json")
            url(url)
        }
    }

    fun HttpClientConfig<*>.setupLogging(config: HttpParams, externalLogger: (message: String) -> Unit) {
        //Logging plugin combined with kermit(KMP Logger library)
        install(Logging) {
            logger = Logger.DEFAULT
            level = config.loggingLevel
            logger = object : Logger {
                override fun log(message: String) {
                    externalLogger(message)
                }
            }
        }
    }

    fun HttpClientConfig<*>.setupContentNegotiation(json: Json) {
        install(ContentNegotiation) {
            json(json = json)
        }
    }
}