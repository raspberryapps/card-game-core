package games.raspberry.card_game.random.data.exceptions

class RandomException(
    val code: Int? = null,
    message: String?,
    cause: Throwable? = null
) : RuntimeException(message, cause) {
    companion object {
        const val REQUEST_FAIL_CODE = 100001
    }
}