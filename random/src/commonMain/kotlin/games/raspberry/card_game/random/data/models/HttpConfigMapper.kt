package games.raspberry.card_game.random.data.models

import games.raspberry.card_game.random.di.HttpParams


fun HttpConfig.mapToParams(): HttpParams {
    return HttpParams(
        baseUrl = baseUrl,
        timeoutMillis = timeoutMillis.toLong(),
    )
}