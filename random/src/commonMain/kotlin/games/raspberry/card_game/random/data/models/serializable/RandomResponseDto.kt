package games.raspberry.card_game.random.data.models.serializable

import kotlinx.serialization.Serializable

@Serializable
internal data class RandomResponseDto<T : RandomResultDto>(
    val jsonrpc: String,
    val result: T,
    val id: String,
)

@Serializable
internal sealed interface RandomResultDto {
    val signature: String?

    @Serializable
    data class IntegersResultDto(
        val random: RandomDataDto.IntegersDataDto,
        override val signature: String?,
        val bitsUsed: Int?,
        val bitsLeft: Int?,
        val requestsLeft: Int?,
        val advisoryDelay: Int?,
    ) : RandomResultDto

    @Serializable
    data class IntegerSequencesResultDto(
        val random: RandomDataDto.IntegerSequencesDataDto,
        override val signature: String?,
        val bitsUsed: Int?,
        val bitsLeft: Int?,
        val requestsLeft: Int?,
        val advisoryDelay: Int?,
    ) : RandomResultDto
}

@Serializable
internal sealed interface RandomDataDto {
    @Serializable
    data class IntegersDataDto(
        val data: List<Int>,
        val completionTime: String,
        val ticketId: String?,
    ) : RandomDataDto

    @Serializable
    data class IntegerSequencesDataDto(
        val data: List<List<Int>>,
        val completionTime: String,
        val ticketId: String?,
    ) : RandomDataDto
}

@Serializable
internal data class TicketDataDto(
    val ticketId: String,
)