package games.raspberry.card_game.random.data.models

import games.raspberry.card_game.random.data.providers.RandomPoolProviderImpl.Companion.CARD_DECK_POOL_SIZE
import games.raspberry.card_game.random.data.providers.RandomPoolProviderImpl.Companion.THRESHOLD_FOR_USED_POOL
import games.raspberry.card_game.random.di.RandomOrgApiProvider.RANDOM_ORG_API_URL
import games.raspberry.card_game.random.di.RandomOrgApiProvider.TEST_RANDOM_ORG_API_KEY
import kotlin.js.JsExport
import kotlin.js.JsName

@JsExport
data class RandomOrgConfig(
    @JsName("apiKey")
    val apiKey: String = TEST_RANDOM_ORG_API_KEY,

    // how many card deck to request each time
    @JsName("poolSize")
    val poolSize: Int = CARD_DECK_POOL_SIZE,
    // min amount of card deck pool should have to be usable
    @JsName("thresholdForUsedPool")
    val thresholdForUsedPool: Int = THRESHOLD_FOR_USED_POOL,
    @JsName("httpConfig")
    val httpConfig: HttpConfig = HttpConfig(
        baseUrl = RANDOM_ORG_API_URL
    ),
)