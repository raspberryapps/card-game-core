package games.raspberry.card_game.random.data.models.serializable

import kotlinx.serialization.Serializable

@Serializable
internal data class ErrorResponseDto(
    val jsonrpc: String,
    val error: ErrorDto?
)

@Serializable
internal data class ErrorDto(
    val code: Int?,
    val message: String?,
)