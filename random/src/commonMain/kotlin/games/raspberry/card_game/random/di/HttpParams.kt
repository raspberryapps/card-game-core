package games.raspberry.card_game.random.di

import io.ktor.client.plugins.logging.*
import kotlinx.serialization.json.Json

data class HttpParams(
    val baseUrl: String,
    val timeoutMillis: Long = 30_000,
    val loggingLevel: LogLevel = LogLevel.ALL,
    val json: Json = Json {
        prettyPrint = true
        isLenient = true
        ignoreUnknownKeys = true
        explicitNulls = false
    },
)