package games.raspberry.card_game.random.di

import games.raspberry.card_game.random.di.ApiCommonConfigs.setupJsonCommon
import io.ktor.client.*
import io.ktor.client.engine.darwin.*
import io.ktor.client.plugins.*

actual object HttpFactory {
    actual fun provideHttpClient(config: HttpParams, externalLogger: (message: String) -> Unit): HttpClient {
        return HttpClient(Darwin) {
            //Timeout plugin to set up timeout milliseconds for client
            install(HttpTimeout) {
                socketTimeoutMillis = config.timeoutMillis
                requestTimeoutMillis = config.timeoutMillis
            }
            setupJsonCommon(config, externalLogger)
        }
    }
}