package io.raspberryapps.card_game.random.utils

import games.raspberry.card_game.random.utils.random.SecureRandom
import games.raspberry.card_game.random.utils.random.overhand
import games.raspberry.card_game.random.utils.random.riffle
import kotlin.test.Test
import kotlin.test.assertNotEquals

internal class ShufflingTest {

    @Test
    fun riffleShuffleTest() {
        val deck = List(32) { it + 1 }
        val iterations = 10
        val random = SecureRandom()
        val newDeck = deck.riffle(iterations, random)
        assertNotEquals(deck, newDeck)
    }

    @Test
    fun overhandShuffleTest() {
        val deck = List(32) { it + 1 }
        val iterations = 10
        val random = SecureRandom()
        val newDeck = deck.overhand(iterations, random)
        assertNotEquals(deck, newDeck)
    }
}