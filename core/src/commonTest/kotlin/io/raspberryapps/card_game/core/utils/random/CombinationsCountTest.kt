package io.raspberryapps.card_game.core.utils.random

import games.raspberry.card_game.random.data.models.CardDeckRequestUserData
import games.raspberry.card_game.random.data.models.RandomOrgConfig
import games.raspberry.card_game.random.data.models.UserInfoData
import games.raspberry.card_game.random.data.providers.PoolParams
import games.raspberry.card_game.random.data.providers.RandomPoolProviderImpl
import games.raspberry.card_game.random.di.RandomOrgApiProvider
import games.raspberry.logger.PlatformRaspberryLogger
import io.raspberryapps.card_game.core.data.models.cards.GameCard
import io.raspberryapps.card_game.core.data.providers.cards.CardsDealerProvider
import io.raspberryapps.card_game.core.data.providers.cards.CardsDealerProviderImpl
import io.raspberryapps.card_game.core.data.providers.combinations.CombinationsCheckerProviderImpl
import io.raspberryapps.card_game.core.domain.dealer.DealerReducerImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import kotlin.test.Test
import kotlin.test.assertTrue


internal class CombinationsCountTest {

    private val rounds = 1000000
    private val logger = PlatformRaspberryLogger()
    private val randomPoolProvider = RandomPoolProviderImpl(
        randomOrgApiService = RandomOrgApiProvider.createApi(
            randomOrgConfig = RandomOrgConfig(
                apiKey = "dddd",
            )
        ),
        logger = PlatformRaspberryLogger(),
        params = PoolParams(),
    )
    private val dealerReducer = DealerReducerImpl(
        logger = logger,
        enableLogs = false
    )
    private val provider = CombinationsCheckerProviderImpl()

    @Test
    fun combinationsCount_WHEN_standardShuffle_AND_secureRandom_AND_twoPlayers_THEN_N_rounds_test() {
        val playersCount = 2
        val cardsDealerProvider = CardsDealerProviderImpl.withSecure(randomPoolProvider, logger)
        val combinationsCounts = mutableMapOf<Int, Int>()
        val playersStat = mutableMapOf<Int, PlayerStat>()

        for (roundIndex in 0 until rounds) {
            val shuffledCards = cardsDealerProvider
                .createNewCardDeck(
                    leftCardIndexForEachSuit = 5,
                    rightCardIndexForEachSuit = 0,
                )
                .let { cardsDealerProvider.standardShuffledCards(it) }

            var cardDeck = shuffledCards
            for (playerIndex in 0 until playersCount) {
                val playerCards = mutableListOf<GameCard>()
                // first round
                val distributeCardsFirstRound = dealerReducer.distributeCards(
                    cards = cardDeck,
                    distributeCards = 6
                )
                playerCards.addAll(distributeCardsFirstRound.distributedCards)
                cardDeck = distributeCardsFirstRound.remainingCardDeck
                // second round
                val distributeCardsSecondRound = dealerReducer.distributeCards(
                    cards = cardDeck,
                    distributeCards = 3
                )
                playerCards.addAll(distributeCardsSecondRound.distributedCards)
                cardDeck = distributeCardsSecondRound.remainingCardDeck

                val combinations = provider.findCardsInRowCombinations(
                    cards = playerCards,
                    supportedCombinations = setOf(
                        3, 4, 7
                    )
                )

                val previousPlayerStat = playersStat[playerIndex] ?: PlayerStat(mutableMapOf(), gamesCount = rounds)
                combinations.forEach {
                    val cardsSize = it.size

                    val previousCount = combinationsCounts[cardsSize] ?: 0
                    combinationsCounts[cardsSize] = previousCount + 1

                    // player stats
                    val previousPlayerCombCount = previousPlayerStat.combinations[cardsSize] ?: 0
                    previousPlayerStat.combinations[cardsSize] = previousPlayerCombCount + 1
                }

                playersStat[playerIndex] = previousPlayerStat
            }
        }

        println(
            "Combinations for $rounds rounds for $playersCount players: \n" +
                    "combinationsCounts: ${
                        combinationsCounts.entries
                            .sortedBy { it.key }
                            .joinToString("") {
                                "   cardsSize: ${it.key}, \n" +
                                        "   count: ${it.value}, \n" +
                                        "   combination exists in %${it.value / (rounds / 100.0)} rounds \n" +
                                        "   player has this combination in %${it.value / (rounds * playersCount / 100.0)} \n" +
                                        "\n"
                            }
                    } \n" +
                    "playerStats: ${playersStat.entries}"
        )

        val threeCardsInRowForRound = combinationsCounts.getValue(3) / (rounds / 100.0)
        val threeCardsInRowForPlayer = combinationsCounts.getValue(3) / (rounds * playersCount / 100.0)
        assertTrue(
            55 < threeCardsInRowForRound && threeCardsInRowForRound < 56,
            "THREE cards in row: wrong combination percentage for round"
        )
        assertTrue(
            27 < threeCardsInRowForPlayer && threeCardsInRowForPlayer < 28,
            "THREE cards in row: wrong combination percentage for player"
        )

        val fourCardsInRowForRound = combinationsCounts.getValue(4) / (rounds / 100.0)
        val fourCardsInRowForPlayer = combinationsCounts.getValue(4) / (rounds * playersCount / 100.0)
        assertTrue(11 < fourCardsInRowForRound && fourCardsInRowForRound < 12.1, "fourCardsInRowForRound is wrong")
        assertTrue(5.9 < fourCardsInRowForPlayer && fourCardsInRowForPlayer < 6.1, "fourCardsInRowForPlayer is wrong")
    }

    @Test
    fun combinationsCount_WHEN_standardShuffle_AND_secureRandom_AND_threePlayers_THEN_N_rounds_test() {
        val playersCount = 3
        val cardsDealerProvider = CardsDealerProviderImpl.withSecure(randomPoolProvider, logger)
        val combinationsCounts = mutableMapOf<Int, Int>()
        val playersStat = mutableMapOf<Int, PlayerStat>()

        for (roundIndex in 0 until rounds) {
            val shuffledCards = cardsDealerProvider
                .createNewCardDeck(
                    leftCardIndexForEachSuit = 5,
                    rightCardIndexForEachSuit = 0,
                )
                .let { cardsDealerProvider.standardShuffledCards(it) }

            var cardDeck = shuffledCards
            for (playerIndex in 0 until playersCount) {
                val playerCards = mutableListOf<GameCard>()
                // first round
                val distributeCardsFirstRound = dealerReducer.distributeCards(
                    cards = cardDeck,
                    distributeCards = 6
                )
                playerCards.addAll(distributeCardsFirstRound.distributedCards)
                cardDeck = distributeCardsFirstRound.remainingCardDeck
                // second round
                val distributeCardsSecondRound = dealerReducer.distributeCards(
                    cards = cardDeck,
                    distributeCards = 3
                )
                playerCards.addAll(distributeCardsSecondRound.distributedCards)
                cardDeck = distributeCardsSecondRound.remainingCardDeck

                val combinations = provider.findCardsInRowCombinations(
                    cards = playerCards,
                    supportedCombinations = setOf(
                        3, 4, 7
                    )
                )

                val previousPlayerStat = playersStat[playerIndex] ?: PlayerStat(mutableMapOf(), gamesCount = rounds)
                combinations.forEach {
                    val cardsSize = it.size

                    val previousCount = combinationsCounts[cardsSize] ?: 0
                    combinationsCounts[cardsSize] = previousCount + 1

                    // player stats
                    val previousPlayerCombCount = previousPlayerStat.combinations[cardsSize] ?: 0
                    previousPlayerStat.combinations[cardsSize] = previousPlayerCombCount + 1
                }

                playersStat[playerIndex] = previousPlayerStat
            }
        }

        println(
            "Combinations for $rounds rounds for $playersCount players: \n" +
                    "combinationsCounts: ${
                        combinationsCounts.entries
                            .sortedBy { it.key }
                            .joinToString("") {
                                "   cardsSize: ${it.key}, \n" +
                                        "   count: ${it.value}, \n" +
                                        "   combination exists in %${it.value / (rounds / 100.0)} rounds \n" +
                                        "   player has this combination in %${it.value / (rounds * playersCount / 100.0)} \n" +
                                        "\n"
                            }
                    } \n" +
                    "playerStats: ${playersStat.entries}"
        )

        val threeCardsInRowForRound = combinationsCounts.getValue(3) / (rounds / 100.0)
        val threeCardsInRowForPlayer = combinationsCounts.getValue(3) / (rounds * playersCount / 100.0)
        assertTrue(
            82 < threeCardsInRowForRound && threeCardsInRowForRound < 83,
            "THREE cards in row: wrong combination percentage for round"
        )
        assertTrue(
            27 < threeCardsInRowForPlayer && threeCardsInRowForPlayer < 28,
            "THREE cards in row: wrong combination percentage for player"
        )

        val fourCardsInRowForRound = combinationsCounts.getValue(4) / (rounds / 100.0)
        val fourCardsInRowForPlayer = combinationsCounts.getValue(4) / (rounds * playersCount / 100.0)
        assertTrue(17 < fourCardsInRowForRound && fourCardsInRowForRound < 19, "fourCardsInRowForRound is wrong")
        assertTrue(5.9 < fourCardsInRowForPlayer && fourCardsInRowForPlayer < 6.1, "fourCardsInRowForPlayer is wrong")
    }

    @Test
    fun combinationsCount_WHEN_standardShuffle_AND_secureRandom_AND_fourPlayers_THEN_N_rounds_test() {
        val playersCount = 4
        val cardsDealerProvider = CardsDealerProviderImpl.withSecure(randomPoolProvider, logger)
        val combinationsCounts = mutableMapOf<Int, Int>()
        val playersStat = mutableMapOf<Int, PlayerStat>()

        for (roundIndex in 0 until rounds) {
            val shuffledCards = cardsDealerProvider
                .createNewCardDeck(
                    leftCardIndexForEachSuit = 5,
                    rightCardIndexForEachSuit = 0,
                )
                .let { cardsDealerProvider.standardShuffledCards(it) }

            var cardDeck = shuffledCards
            for (playerIndex in 0 until playersCount) {
                val playerCards = mutableListOf<GameCard>()
                // first round
                val distributeCardsFirstRound = dealerReducer.distributeCards(
                    cards = cardDeck,
                    distributeCards = 6
                )
                playerCards.addAll(distributeCardsFirstRound.distributedCards)
                cardDeck = distributeCardsFirstRound.remainingCardDeck
                // second round
                val distributeCardsSecondRound = dealerReducer.distributeCards(
                    cards = cardDeck,
                    distributeCards = 2
                )
                playerCards.addAll(distributeCardsSecondRound.distributedCards)
                cardDeck = distributeCardsSecondRound.remainingCardDeck

                val combinations = provider.findCardsInRowCombinations(
                    cards = playerCards,
                    supportedCombinations = setOf(
                        3, 4, 7
                    )
                )

                val previousPlayerStat = playersStat[playerIndex] ?: PlayerStat(mutableMapOf(), gamesCount = rounds)
                combinations.forEach {
                    val cardsSize = it.size

                    val previousCount = combinationsCounts[cardsSize] ?: 0
                    combinationsCounts[cardsSize] = previousCount + 1

                    // player stats
                    val previousPlayerCombCount = previousPlayerStat.combinations[cardsSize] ?: 0
                    previousPlayerStat.combinations[cardsSize] = previousPlayerCombCount + 1
                }

                playersStat[playerIndex] = previousPlayerStat
            }
        }

        println(
            "Combinations for $rounds rounds for $playersCount players: \n" +
                    "combinationsCounts: ${
                        combinationsCounts.entries
                            .sortedBy { it.key }
                            .joinToString("") {
                                "   cardsSize: ${it.key}, \n" +
                                        "   count: ${it.value}, \n" +
                                        "   combination exists in %${it.value / (rounds / 100.0)} rounds \n" +
                                        "   player has this combination in %${it.value / (rounds * playersCount / 100.0)} \n" +
                                        "\n"
                            }
                    } \n" +
                    "playerStats: ${playersStat.entries}"
        )

        val threeCardsInRowForRound = combinationsCounts.getValue(3) / (rounds / 100.0)
        val threeCardsInRowForPlayer = combinationsCounts.getValue(3) / (rounds * playersCount / 100.0)
        assertTrue(
            78 < threeCardsInRowForRound && threeCardsInRowForRound < 80,
            "THREE cards in row: wrong combination percentage for round"
        )
        assertTrue(
            19 < threeCardsInRowForPlayer && threeCardsInRowForPlayer < 20,
            "THREE cards in row: wrong combination percentage for player"
        )

        val fourCardsInRowForRound = combinationsCounts.getValue(4) / (rounds / 100.0)
        val fourCardsInRowForPlayer = combinationsCounts.getValue(4) / (rounds * playersCount / 100.0)
        assertTrue(13 < fourCardsInRowForRound && fourCardsInRowForRound < 14, "fourCardsInRowForRound is wrong")
        assertTrue(3.3 < fourCardsInRowForPlayer && fourCardsInRowForPlayer < 3.6, "fourCardsInRowForPlayer is wrong")
    }

    @Test
    fun combinationsCount_WHEN_standardShuffle_AND_timerandom_AND_fourPlayers_THEN_N_rounds_test() {
        val playersCount = 4
        val cardsDealerProvider = CardsDealerProviderImpl.withTimeSeed(randomPoolProvider, logger)
        val combinationsCounts = mutableMapOf<Int, Int>()

        for (roundIndex in 0 until rounds) {
            val shuffledCards = cardsDealerProvider
                .createNewCardDeck(
                    leftCardIndexForEachSuit = 5,
                    rightCardIndexForEachSuit = 0,
                )
                .let { cardsDealerProvider.standardShuffledCards(it) }

            var cardDeck = shuffledCards
            for (playerIndex in 0 until playersCount) {
                val playerCards = mutableListOf<GameCard>()
                // first round
                val distributeCardsFirstRound = dealerReducer.distributeCards(
                    cards = cardDeck,
                    distributeCards = 6
                )
                playerCards.addAll(distributeCardsFirstRound.distributedCards)
                cardDeck = distributeCardsFirstRound.remainingCardDeck
                // second round
                val distributeCardsSecondRound = dealerReducer.distributeCards(
                    cards = cardDeck,
                    distributeCards = 2
                )
                playerCards.addAll(distributeCardsSecondRound.distributedCards)
                cardDeck = distributeCardsSecondRound.remainingCardDeck

                val combinations = provider.findCardsInRowCombinations(
                    cards = playerCards,
                    supportedCombinations = setOf(
                        3, 4,// 7
                    )
                )

                combinations.forEach {
                    val cardsSize = it.size
                    val previousCount = combinationsCounts[cardsSize] ?: 0
                    combinationsCounts[cardsSize] = previousCount + 1
                }
            }
        }

        println(
            "Combinations for $rounds rounds for $playersCount players: \n${
                combinationsCounts.entries
                    .sortedBy { it.key }
                    .joinToString("") {
                        "   cardsSize: ${it.key}, \n" +
                                "   count: ${it.value}, \n" +
                                "   combination exists in %${it.value / (rounds / 100.0)} rounds \n" +
                                "   player has this combination in %${it.value / (rounds * playersCount / 100.0)} \n" +
                                "\n"
                    }
            }"
        )

        val threeCardsInRowForRound = combinationsCounts.getValue(3) / (rounds / 100.0)
        val threeCardsInRowForPlayer = combinationsCounts.getValue(3) / (rounds * playersCount / 100.0)
        assertTrue(78 < threeCardsInRowForRound && threeCardsInRowForRound < 80, "threeCardsInRowForRound is wrong")
        assertTrue(19 < threeCardsInRowForPlayer && threeCardsInRowForPlayer < 20, "threeCardsInRowForPlayer is wrong")

        val fourCardsInRowForRound = combinationsCounts.getValue(4) / (rounds / 100.0)
        val fourCardsInRowForPlayer = combinationsCounts.getValue(4) / (rounds * playersCount / 100.0)
        assertTrue(13 < fourCardsInRowForRound && fourCardsInRowForRound < 14, "fourCardsInRowForRound is wrong")
        assertTrue(3.3 < fourCardsInRowForPlayer && fourCardsInRowForPlayer < 3.6, "fourCardsInRowForPlayer is wrong")
    }

    @Test
    fun combinationsCount_WHEN_customRandomShuffle_AND_fourPlayers_THEN_N_rounds_test() = runTest {
        val playersCount = 4
        val cardsDealerProvider = CardsDealerProviderImpl.withTimeSeed(randomPoolProvider, logger)
        val combinationsCounts = mutableMapOf<Int, Int>()

        for (roundIndex in 0 until rounds) {
            val shuffledCards = cardsDealerProvider
                .createNewShuffledCardDeck(
                    leftCardIndexForEachSuit = 5,
                    rightCardIndexForEachSuit = 0,
                    approximateRoundCountForGame = 3,
                    userData = CardDeckRequestUserData(
                        players = List(playersCount) {
                            UserInfoData(id = "player_$it", name = "Test user $it")
                        },
                        gameId = "gameId_$roundIndex",
                    ),
                    provider = CardsDealerProvider.CardsRandomProvider.CUSTOM,
                )
                .let { cardsDealerProvider.standardShuffledCards(it) }

            var cardDeck = shuffledCards
            for (playerIndex in 0 until playersCount) {
                val playerCards = mutableListOf<GameCard>()
                // first round
                val distributeCardsFirstRound = dealerReducer.distributeCards(
                    cards = cardDeck,
                    distributeCards = 6
                )
                playerCards.addAll(distributeCardsFirstRound.distributedCards)
                cardDeck = distributeCardsFirstRound.remainingCardDeck
                // second round
                val distributeCardsSecondRound = dealerReducer.distributeCards(
                    cards = cardDeck,
                    distributeCards = 2
                )
                playerCards.addAll(distributeCardsSecondRound.distributedCards)
                cardDeck = distributeCardsSecondRound.remainingCardDeck

                val combinations = provider.findCardsInRowCombinations(
                    cards = playerCards,
                    supportedCombinations = setOf(
                        3, 4,// 7
                    )
                )

                combinations.forEach {
                    val cardsSize = it.size
                    val previousCount = combinationsCounts[cardsSize] ?: 0
                    combinationsCounts[cardsSize] = previousCount + 1
                }
            }
        }

        println(
            "Combinations for $rounds rounds for $playersCount players: \n${
                combinationsCounts.entries
                    .sortedBy { it.key }
                    .joinToString("") {
                        "   cardsSize: ${it.key}, \n" +
                                "   count: ${it.value}, \n" +
                                "   combination exists in %${it.value / (rounds / 100.0)} rounds \n" +
                                "   player has this combination in %${it.value / (rounds * playersCount / 100.0)} \n" +
                                "\n"
                    }
            }"
        )

        val threeCardsInRowForRound = combinationsCounts.getValue(3) / (rounds / 100.0)
        val threeCardsInRowForPlayer = combinationsCounts.getValue(3) / (rounds * playersCount / 100.0)
        assertTrue(78 < threeCardsInRowForRound && threeCardsInRowForRound < 80, "threeCardsInRowForRound is wrong")
        assertTrue(19 < threeCardsInRowForPlayer && threeCardsInRowForPlayer < 20, "threeCardsInRowForPlayer is wrong")

        val fourCardsInRowForRound = combinationsCounts.getValue(4) / (rounds / 100.0)
        val fourCardsInRowForPlayer = combinationsCounts.getValue(4) / (rounds * playersCount / 100.0)
        assertTrue(13 < fourCardsInRowForRound && fourCardsInRowForRound < 14, "fourCardsInRowForRound is wrong")
        assertTrue(3.3 < fourCardsInRowForPlayer && fourCardsInRowForPlayer < 3.6, "fourCardsInRowForPlayer is wrong")
    }

    @Test
    fun combinationsCount_WHEN_randomOrgShuffle_AND_fourPlayers_THEN_N_rounds_test() = runTest {
        val playersCount = 4
        val cardsDealerProvider = CardsDealerProviderImpl.withTimeSeed(randomPoolProvider, logger)
        val combinationsCounts = mutableMapOf<Int, Int>()

        val testRounds = 100 // to reduce number of API calls

        for (roundIndex in 0 until testRounds) {
            val shuffledCards = withContext(Dispatchers.Default) {
                cardsDealerProvider
                    .createNewShuffledCardDeck(
                        leftCardIndexForEachSuit = 5,
                        rightCardIndexForEachSuit = 0,
                        approximateRoundCountForGame = 3,
                        userData = CardDeckRequestUserData(
                            players = List(playersCount) {
                                UserInfoData(id = "player_$it", name = "Test user $it")
                            },
                            gameId = "gameId_$roundIndex",
                        ),
                        provider = CardsDealerProvider.CardsRandomProvider.RANDOM_ORG,
                    )
                    .let { cardsDealerProvider.standardShuffledCards(it) }
            }

            var cardDeck = shuffledCards
            for (playerIndex in 0 until playersCount) {
                val playerCards = mutableListOf<GameCard>()
                // first round
                val distributeCardsFirstRound = dealerReducer.distributeCards(
                    cards = cardDeck,
                    distributeCards = 6
                )
                playerCards.addAll(distributeCardsFirstRound.distributedCards)
                cardDeck = distributeCardsFirstRound.remainingCardDeck
                // second round
                val distributeCardsSecondRound = dealerReducer.distributeCards(
                    cards = cardDeck,
                    distributeCards = 2
                )
                playerCards.addAll(distributeCardsSecondRound.distributedCards)
                cardDeck = distributeCardsSecondRound.remainingCardDeck

                val combinations = provider.findCardsInRowCombinations(
                    cards = playerCards,
                    supportedCombinations = setOf(
                        3, 4,// 7
                    )
                )

                combinations.forEach {
                    val cardsSize = it.size
                    val previousCount = combinationsCounts[cardsSize] ?: 0
                    combinationsCounts[cardsSize] = previousCount + 1
                }
            }
        }
        println(
            "Combinations for $testRounds rounds for $playersCount players: \n${
                combinationsCounts.entries
                    .sortedBy { it.key }
                    .joinToString("") {
                        "   cardsSize: ${it.key}, \n" +
                                "   count: ${it.value}, \n" +
                                "   combination exists in %${it.value / (testRounds / 100.0)} rounds \n" +
                                "   player has this combination in %${it.value / (testRounds * playersCount / 100.0)} \n" +
                                "\n"
                    }
            }"
        )

        val threeCardsInRowForRound = combinationsCounts.getValue(3) / (testRounds / 100.0)
        val threeCardsInRowForPlayer = combinationsCounts.getValue(3) / (testRounds * playersCount / 100.0)
        assertTrue(78 < threeCardsInRowForRound && threeCardsInRowForRound < 80, "threeCardsInRowForRound is wrong")
        assertTrue(19 < threeCardsInRowForPlayer && threeCardsInRowForPlayer < 20, "threeCardsInRowForPlayer is wrong")

        val fourCardsInRowForRound = combinationsCounts.getValue(4) / (testRounds / 100.0)
        val fourCardsInRowForPlayer = combinationsCounts.getValue(4) / (testRounds * playersCount / 100.0)
        assertTrue(13 < fourCardsInRowForRound && fourCardsInRowForRound < 14, "fourCardsInRowForRound is wrong")
        assertTrue(3.3 < fourCardsInRowForPlayer && fourCardsInRowForPlayer < 3.6, "fourCardsInRowForPlayer is wrong")
    }

    data class PlayerStat(
        val combinations: MutableMap<Int, Int>,
        val gamesCount: Int,
    ) {

        private fun getPointsByGames(): Int {
            return getPoints() / gamesCount
        }

        private fun getPoints(): Int {
            return combinations
                .map { it.key.getPoints() * (if (it.value == 0) 1 else it.value) }
                .sum()
        }

        private fun Int.getPoints(): Int {
            return when (this) {
                3 -> 20
                4 -> 50
                7 -> 1001
                else -> throw Exception("Not supported")
            }
        }

        override fun toString(): String {
            return "PlayerStat(pointsPerRound: ${getPointsByGames()})"
        }
    }
}