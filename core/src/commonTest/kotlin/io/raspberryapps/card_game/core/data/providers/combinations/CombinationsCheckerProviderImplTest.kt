package io.raspberryapps.card_game.core.data.providers.combinations

import io.raspberryapps.card_game.core.data.models.cards.GameCard
import io.raspberryapps.card_game.core.data.models.cards.Suit
import kotlin.test.Test
import kotlin.test.assertEquals


internal class CombinationsCheckerProviderImplTest {

    private val provider = CombinationsCheckerProviderImpl()

    @Test
    fun single3CardsCombination_test() {
        // GIVEN
        val cards = listOf(
            // 3 in row
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.FIVE),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SIX),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),

            // other
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.KING),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.DAME),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.TEN),
        )

        // WHEN
        val combinations = provider.findCardsInRowCombinations(
            cards = cards.shuffled(),
            supportedCombinations = setOf(3, 4, 5),
        )

        // THEN
        assertEquals(combinations.size, 1)
        assertEquals(
            combinations.first(), listOf(
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.FIVE),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SIX),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),
            )
        )
    }

    @Test
    fun single4CardsCombination_test() {
        // GIVEN
        val cards = listOf(
            // 4 in row
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.FIVE),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SIX),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.EIGHT),

            // other
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.KING),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.DAME),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.TEN),
        )

        // WHEN
        val combinations = provider.findCardsInRowCombinations(
            cards = cards.shuffled(),
            supportedCombinations = setOf(3, 4, 5),
        )

        // THEN
        assertEquals(combinations.size, 1)
        assertEquals(
            combinations.first(), listOf(
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.FIVE),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SIX),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.EIGHT),
            )
        )
    }

    @Test
    fun single5CardsCombination_test() {
        // GIVEN
        val cards = listOf(
            // 5 in row
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.FIVE),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SIX),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.EIGHT),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.NINE),

            // other
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.KING),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.DAME),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.TEN),
        )

        // WHEN
        val combinations = provider.findCardsInRowCombinations(
            cards = cards.shuffled(),
            supportedCombinations = setOf(3, 4, 5),
        )

        // THEN
        assertEquals(combinations.size, 1)
        assertEquals(
            combinations.first(), listOf(
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.FIVE),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SIX),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.EIGHT),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.NINE),
            )
        )
    }

    @Test
    fun cardsInRowMoreThenSupported_THEN_ReturnHighestCombination_test() {
        // GIVEN
        val cards = listOf(
            // 7 in row
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.FIVE),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SIX),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.EIGHT),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.NINE),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.TEN),

            // other
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.KING),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.DAME),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.TEN),
        )

        // WHEN
        val combinations = provider.findCardsInRowCombinations(
            cards = cards.shuffled(),
            supportedCombinations = setOf(3, 4, 5),
        )

        // THEN
        assertEquals(combinations.size, 1)
        assertEquals(
            combinations.first(), listOf(
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SIX),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.EIGHT),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.NINE),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.TEN),
            )
        )
    }

    @Test
    fun cardsInRowNotSupported_THEN_returnHighestCombination_test() {
        // GIVEN
        val cards = listOf(
            // 4 in row
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.FIVE),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SIX),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.EIGHT),

            // other
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.KING),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.DAME),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.TEN),
        )

        // WHEN
        val combinations = provider.findCardsInRowCombinations(
            cards = cards.shuffled(),
            supportedCombinations = setOf(3, 5),
        )

        // THEN
        assertEquals(combinations.size, 1)
        assertEquals(
            combinations.first(), listOf(
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SIX),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.EIGHT),
            )
        )
    }

    @Test
    fun twoCombination_WHEN_differentSuit_test() {
        // GIVEN
        val cards = listOf(
            // 3 in row
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.FIVE),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SIX),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),

            // 3 in row
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.FIVE),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.SIX),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.SEVEN),

            // other
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.ACE),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.KING),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.DAME),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.TEN),
        )

        // WHEN
        val combinations = provider.findCardsInRowCombinations(
            cards = cards.shuffled(),
            supportedCombinations = setOf(3, 5),
        )

        // THEN
        assertEquals(combinations.size, 2)
        assertEquals(
            combinations.first(),
            listOf(
                GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.FIVE),
                GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.SIX),
                GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.SEVEN),
            ),
        )
        assertEquals(
            combinations[1],
            listOf(
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.FIVE),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SIX),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),
            ),
        )
    }

    @Test
    fun threeCombination_WHEN_differentSuit_test() {
        // GIVEN
        val cards = listOf(
            // 3 in row
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.FIVE),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SIX),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),

            // 3 in row
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.FIVE),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.SIX),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.SEVEN),

            // 3 in row
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.DAME),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.KING),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.ACE),

            // other
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.ACE),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.NINE),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.TEN),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.TEN),
        )

        // WHEN
        val combinations = provider.findCardsInRowCombinations(
            cards = cards.shuffled(),
            supportedCombinations = setOf(3, 5),
        )

        // THEN
        assertEquals(combinations.size, 3)
        assertEquals(
            combinations.first(),
            listOf(
                GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.FIVE),
                GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.SIX),
                GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.SEVEN),
            ),
        )
        assertEquals(
            combinations[1],
            listOf(
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.FIVE),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SIX),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),
            ),
        )
        assertEquals(
            combinations[2],
            listOf(
                GameCard.Factory.create(Suit.HEART, GameCard.CardName.DAME),
                GameCard.Factory.create(Suit.HEART, GameCard.CardName.KING),
                GameCard.Factory.create(Suit.HEART, GameCard.CardName.ACE),
            ),
        )
    }

    @Test
    fun twoCombination_WHEN_sameSuit_test() {
        // GIVEN
        val cards = listOf(
            // 3 in row
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.FIVE),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SIX),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),

            // 3 in row
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.NINE),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.TEN),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.JACK),

            // other
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.ACE),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.KING),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.DAME),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.TEN),
        )

        // WHEN
        val combinations = provider.findCardsInRowCombinations(
            cards = cards.shuffled(),
            supportedCombinations = setOf(3, 5),
        )

        // THEN
        assertEquals(combinations.size, 2)
        assertEquals(
            combinations.first(),
            listOf(
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.FIVE),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SIX),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),
            ),
        )
        assertEquals(
            combinations[1],
            listOf(
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.NINE),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.TEN),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.JACK),
            ),
        )
    }

    @Test
    fun twoCombination_WHEN_sameSuit_BUT_notDivided_test() {
        // GIVEN
        val cards = listOf(
            // 3 in row
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.FIVE),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SIX),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),

            // 4 in row
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.EIGHT),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.NINE),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.TEN),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.JACK),

            // other
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.ACE),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.KING),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.DAME),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.TEN),
        )

        // WHEN
        val combinations = provider.findCardsInRowCombinations(
            cards = cards.shuffled(),
            supportedCombinations = setOf(3, 4),
        )

        // THEN
        assertEquals(combinations.size, 2)
        assertEquals(
            combinations.first(),
            listOf(
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.EIGHT),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.NINE),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.TEN),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.JACK),
            ),
        )
        assertEquals(
            combinations[1],
            listOf(
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.FIVE),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SIX),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),
            ),
        )
    }

    @Test
    fun bellotCombination_test() {
        // GIVEN
        val cards = listOf(
            // combinations
            // 2 in row
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.KING),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.DAME),

            // 3 in row
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.EIGHT),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.NINE),

            // other
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.TEN),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.JACK),
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.ACE),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.KING),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.DAME),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.TEN),
        )

        // WHEN
        val combinations = provider.findCardsInRowCombinations(
            cards = cards.shuffled(),
            supportedCombinations = setOf(2),
            suit = Suit.SPADE,
            includeCardNames = setOf(GameCard.CardName.KING, GameCard.CardName.DAME)
        )

        // THEN
        assertEquals(combinations.size, 1)
        assertEquals(
            combinations.first(),
            listOf(
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.DAME),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.KING),
            ),
        )
    }

    @Test
    fun bellotCombinationLastCardToCheck_test() {
        // GIVEN
        val cards = listOf(
            // combinations
            // 2 in row
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.KING),
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.DAME),

            // 3 in row
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.DAME),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.TEN),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.JACK),
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.TEN),

            // other
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.TEN),
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.SEVEN),
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.NINE),
        )

        // WHEN
        val combinations = provider.findCardsInRowCombinations(
            cards = cards.shuffled(),
            supportedCombinations = setOf(2),
            suit = Suit.CLUB,
            includeCardNames = setOf(GameCard.CardName.KING, GameCard.CardName.DAME)
        )

        // THEN
        assertEquals(combinations.size, 1)
        assertEquals(
            combinations.first(),
            listOf(
                GameCard.Factory.create(Suit.CLUB, GameCard.CardName.DAME),
                GameCard.Factory.create(Suit.CLUB, GameCard.CardName.KING),
            ),
        )
    }

    @Test
    fun EIGHT_CARDS_IN_ROW_CombinationLastCardToCheck_test() {
        // GIVEN
        val cards = listOf(
            // combinations
            // 8 in row
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.EIGHT),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.NINE),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.TEN),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.JACK),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.DAME),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.KING),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.ACE),

            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.ACE),
        )

        // WHEN
        val combinations = provider.findCardsInRowCombinations(
            cards = cards.shuffled(),
            supportedCombinations = setOf(3, 4, 7),
        )

        // THEN
        assertEquals(combinations.size, 1)
        assertEquals(
            combinations.first(),
            listOf(
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.EIGHT),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.NINE),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.TEN),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.JACK),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.DAME),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.KING),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.ACE),
            ),
        )
    }

    @Test
    fun FOUR_JACK_Combination_test() {
        // GIVEN
        val cards = listOf(
            // combinations
            // 4 Jacks
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.JACK),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.JACK),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.JACK),
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.JACK),

            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.DAME),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.KING),
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.ACE),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.ACE),
        )

        // WHEN
        val combinations = provider.findSameCardsNumberCombinations(
            cards = cards.shuffled(),
            includeCardNames = setOf(GameCard.CardName.JACK),
        )

        // THEN
        assertEquals(combinations.size, 1)
        assertEquals(
            combinations.first(),
            listOf(
                GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.JACK),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.JACK),
                GameCard.Factory.create(Suit.HEART, GameCard.CardName.JACK),
                GameCard.Factory.create(Suit.CLUB, GameCard.CardName.JACK),
            ),
        )
    }

    @Test
    fun FOUR_JACK_AND_FOUR_NINES_Combination_test() {
        // GIVEN
        val cards = listOf(
            // combinations
            // 4 Jacks
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.JACK),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.JACK),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.JACK),
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.JACK),

            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.NINE),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.NINE),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.NINE),
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.NINE),
        )

        // WHEN
        val combinations = provider.findSameCardsNumberCombinations(
            cards = cards.shuffled(),
            includeCardNames = setOf(GameCard.CardName.JACK, GameCard.CardName.NINE),
        )

        // THEN
        assertEquals(combinations.size, 2)
        assertEquals(
            expected = combinations,
            actual = listOf(
                listOf(
                    GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.JACK),
                    GameCard.Factory.create(Suit.SPADE, GameCard.CardName.JACK),
                    GameCard.Factory.create(Suit.HEART, GameCard.CardName.JACK),
                    GameCard.Factory.create(Suit.CLUB, GameCard.CardName.JACK),
                ),
                listOf(
                    GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.NINE),
                    GameCard.Factory.create(Suit.SPADE, GameCard.CardName.NINE),
                    GameCard.Factory.create(Suit.HEART, GameCard.CardName.NINE),
                    GameCard.Factory.create(Suit.CLUB, GameCard.CardName.NINE),
                ),
            ),
        )
    }

    @Test
    fun FOUR_JACK_AND_NotSupported_FOUR_EIGHTS_Combination_test() {
        // GIVEN
        val cards = listOf(
            // combinations
            // 4 Jacks
            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.JACK),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.JACK),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.JACK),
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.JACK),

            GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.NINE),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.NINE),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.NINE),
            GameCard.Factory.create(Suit.CLUB, GameCard.CardName.NINE),
        )

        // WHEN
        val combinations = provider.findSameCardsNumberCombinations(
            cards = cards.shuffled(),
            includeCardNames = setOf(GameCard.CardName.JACK),
        )

        // THEN
        assertEquals(combinations.size, 1)
        assertEquals(
            combinations.first(),
            listOf(
                GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.JACK),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.JACK),
                GameCard.Factory.create(Suit.HEART, GameCard.CardName.JACK),
                GameCard.Factory.create(Suit.CLUB, GameCard.CardName.JACK),
            ),
        )
    }

    @Test
    fun DoubleDebertz_Combination_test() {
        // GIVEN
        val cards = listOf(
            // combinations
            // 4 Jacks
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.ACE),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.KING),
            GameCard.Factory.create(Suit.SPADE, GameCard.CardName.KING),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.DAME),

            GameCard.Factory.create(Suit.HEART, GameCard.CardName.JACK),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.TEN),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.NINE),
            GameCard.Factory.create(Suit.HEART, GameCard.CardName.EIGHT),
        )

        // WHEN
        val combinations = provider.findCardsInRowCombinations(
            cards = cards.shuffled(),
            supportedCombinations = setOf(3, 4, 7),
        )

        // THEN
        assertEquals(combinations.size, 1)
        assertEquals(
            combinations.first(),
            listOf(
                GameCard.Factory.create(Suit.HEART, GameCard.CardName.EIGHT),
                GameCard.Factory.create(Suit.HEART, GameCard.CardName.NINE),
                GameCard.Factory.create(Suit.HEART, GameCard.CardName.TEN),
                GameCard.Factory.create(Suit.HEART, GameCard.CardName.JACK),
                GameCard.Factory.create(Suit.HEART, GameCard.CardName.DAME),
                GameCard.Factory.create(Suit.HEART, GameCard.CardName.KING),
                GameCard.Factory.create(Suit.HEART, GameCard.CardName.ACE),
            ),
        )
    }
}