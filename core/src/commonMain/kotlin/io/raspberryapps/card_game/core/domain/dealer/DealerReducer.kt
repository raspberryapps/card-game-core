package io.raspberryapps.card_game.core.domain.dealer

import io.raspberryapps.card_game.core.data.models.cards.GameCard
import io.raspberryapps.card_game.core.data.models.cards.distributeCardsBottom
import io.raspberryapps.card_game.core.domain.dealer.DealerReducerImpl.DealerDistribution
import games.raspberry.logger.RaspberryLogger

interface DealerReducer {
    fun distributeCards(cards: List<GameCard>, distributeCards: Int): DealerDistribution
    fun removeCards(cards: List<GameCard>, cardsToRemove: List<GameCard>): List<GameCard>
    fun removeCard(cards: List<GameCard>, cardToRemove: GameCard): List<GameCard>
    fun addCard(cards: List<GameCard>, cardToAdd: GameCard): List<GameCard>

    companion object {
        const val TAG = "Dealer"
    }
}

class DealerReducerImpl(
    private val logger: RaspberryLogger,
    private val enableLogs: Boolean,
) : DealerReducer {

    override fun distributeCards(cards: List<GameCard>, distributeCards: Int): DealerDistribution {
        val cardsInTheCardDeck = cards.toMutableList()
        return DealerDistribution(
            remainingCardDeck = cardsInTheCardDeck,
            distributedCards = cardsInTheCardDeck.distributeCardsBottom(
                distributeCards
            ),
        )
    }

    override fun removeCards(cards: List<GameCard>, cardsToRemove: List<GameCard>): List<GameCard> {
        var list = cards
        for (gameCard in cardsToRemove) {
            list = removeCard(list, gameCard)
        }
        return list
    }

    override fun removeCard(cards: List<GameCard>, cardToRemove: GameCard): List<GameCard> =
        if (!cardToRemove.isStub && cards.contains(cardToRemove)) {
            if (enableLogs) {
                logger.d(tag = DealerReducer.TAG, message = "Card removed: $cardToRemove, size: ${cards.size - 1}")
            }
            cards - cardToRemove
        } else {
            if (enableLogs) {
                logger.d(
                    tag = DealerReducer.TAG,
                    message = "Card removed as stub: $cardToRemove, size: ${cards.size - 1}"
                )
            }
            val notStubCards = cards.filterNot { it.isStub }
            val stubCards = cards.filter { it.isStub }
            if (stubCards.isEmpty()) {
                logger.w(
                    tag = DealerReducer.TAG,
                    message = "Can't find card to delete for player, " +
                            "cards:$cards, " +
                            "cardToRemove: $cardToRemove"
                )
            }
            notStubCards + stubCards.dropLast(1)
        }

    override fun addCard(cards: List<GameCard>, cardToAdd: GameCard): List<GameCard> {
        if (enableLogs) {
            logger.d(tag = DealerReducer.TAG, message = "Card added: $cardToAdd, size: ${cards.size + 1}")
        }
        if (cards.contains(cardToAdd)) {
            logger.w(
                tag = DealerReducer.TAG,
                message = "Card already exists for player, " +
                        "cards:$cards, " +
                        "cardToAdd: $cardToAdd"
            )
        }
        return cards + cardToAdd
    }

    data class DealerDistribution(
        val remainingCardDeck: List<GameCard>,
        val distributedCards: List<GameCard>
    )
}