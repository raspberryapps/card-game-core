package io.raspberryapps.card_game.core.data.providers.combinations

import io.raspberryapps.card_game.core.data.models.cards.GameCard
import io.raspberryapps.card_game.core.data.models.cards.Suit

interface CombinationsCheckerProvider {
    fun findCardsInRowCombinations(
        cards: List<GameCard>,
        supportedCombinations: Set<Int>,
        checkFromIndex: Int = supportedCombinations.first(),
        includeCardNames: Set<GameCard.CardName>? = null,
        suit: Suit? = null,
    ): List<List<GameCard>>

    fun findSameCardsNumberCombinations(
        cards: List<GameCard>,
        cardsCount: Int = 4,
        includeCardNames: Set<GameCard.CardName>? = null,
    ): List<List<GameCard>>

    fun createCombinationsFromCardsInRow(
        cards: List<GameCard>,
        supportedCombinations: Set<Int>,
    ): List<List<GameCard>>
}