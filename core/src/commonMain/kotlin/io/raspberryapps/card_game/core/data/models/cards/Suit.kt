package io.raspberryapps.card_game.core.data.models.cards

import kotlinx.serialization.Serializable

@Serializable
enum class Suit {
    // TODO swap suits https://busycats.atlassian.net/browse/DEB-419
    DIAMOND, SPADE, HEART, CLUB;

    override fun toString(): String {
        return getSuitEmoji()
    }

    private fun getSuitEmoji(): String {
        return when (this) {
            DIAMOND -> "♦️"
            SPADE -> "♣️️"
            HEART -> "♥️"
            CLUB -> "♠️"
        }
    }
}