package io.raspberryapps.card_game.core.data.providers.cards

import games.raspberry.card_game.random.data.models.CardDeckRequestUserData
import io.raspberryapps.card_game.core.data.models.cards.GameCard
import io.raspberryapps.card_game.core.data.models.cards.Suit

class FakeCardsDealerProvider(
    private val cards: List<GameCard> = getCards(),
    private val cardsForLot: List<GameCard> = getCardsForLot()
) : CardsDealerProvider {

    override fun createNewCardDeck(leftCardIndexForEachSuit: Int, rightCardIndexForEachSuit: Int): List<GameCard> {
        return cards
    }

    override suspend fun createNewShuffledCardDeck(
        leftCardIndexForEachSuit: Int,
        rightCardIndexForEachSuit: Int,
        approximateRoundCountForGame: Int,
        userData: CardDeckRequestUserData,
        provider: CardsDealerProvider.CardsRandomProvider
    ): List<GameCard> {
        return cards
    }

    override fun getNRandomUniqueCards(count: Int, cards: List<GameCard>): List<GameCard> {
        return cardsForLot
    }

    override fun standardShuffledCards(cards: List<GameCard>, times: Int): List<GameCard> {
        return cards
    }

    override fun realLifeShuffledCards(cards: List<GameCard>, times: Int): List<GameCard> {
        return cards
    }

    override fun getCardsCount(leftCardIndexForEachSuit: Int, rightCardIndexForEachSuit: Int): Int {
        return cards.size
    }

    override fun getCardsRange(leftCardIndexForEachSuit: Int, rightCardIndexForEachSuit: Int): IntRange {
        return (leftCardIndexForEachSuit until GameCard.SUIT_CARD_LINE_CARDS_SIZE - rightCardIndexForEachSuit)
    }

    companion object {
        fun getCards(): List<GameCard> {
            return mutableListOf<GameCard>().apply {
                // player 1
                addAll(
                    listOf(
                        GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.SEVEN),
                        GameCard.Factory.create(Suit.CLUB, GameCard.CardName.NINE),
                        GameCard.Factory.create(Suit.CLUB, GameCard.CardName.SEVEN),
                        GameCard.Factory.create(Suit.CLUB, GameCard.CardName.TEN),
                        GameCard.Factory.create(Suit.HEART, GameCard.CardName.JACK),
                        GameCard.Factory.create(Suit.HEART, GameCard.CardName.KING),
                    )
                )

                // player 2
                addAll(
                    listOf(
                        GameCard.Factory.create(Suit.CLUB, GameCard.CardName.JACK),
                        GameCard.Factory.create(Suit.SPADE, GameCard.CardName.ACE),
                        GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.NINE),
                        GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.TEN),
                        GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.JACK),
                        GameCard.Factory.create(Suit.HEART, GameCard.CardName.EIGHT),
                    )
                )

                // player 3
                addAll(
                    listOf(
                        GameCard.Factory.create(Suit.CLUB, GameCard.CardName.DAME),
                        GameCard.Factory.create(Suit.HEART, GameCard.CardName.ACE),
                        GameCard.Factory.create(Suit.HEART, GameCard.CardName.DAME),
                        GameCard.Factory.create(Suit.HEART, GameCard.CardName.SEVEN),
                        GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),
                        GameCard.Factory.create(Suit.CLUB, GameCard.CardName.KING),
                    )
                )

                // player 4
                addAll(
                    listOf(
                        GameCard.Factory.create(Suit.SPADE, GameCard.CardName.EIGHT),
                        GameCard.Factory.create(Suit.SPADE, GameCard.CardName.TEN),
                        GameCard.Factory.create(Suit.HEART, GameCard.CardName.TEN),
                        GameCard.Factory.create(Suit.SPADE, GameCard.CardName.JACK),
                        GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.EIGHT),
                        GameCard.Factory.create(Suit.SPADE, GameCard.CardName.DAME),
                    )
                )

                // second round
                addAll(
                    listOf(
                        GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.ACE),
                        GameCard.Factory.create(Suit.CLUB, GameCard.CardName.EIGHT),
                        GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.KING),
                        GameCard.Factory.create(Suit.SPADE, GameCard.CardName.NINE),
                        GameCard.Factory.create(Suit.CLUB, GameCard.CardName.ACE),
                        GameCard.Factory.create(Suit.SPADE, GameCard.CardName.KING),
                        GameCard.Factory.create(Suit.DIAMOND, GameCard.CardName.DAME),
                        GameCard.Factory.create(Suit.HEART, GameCard.CardName.NINE),
                    )
                )
            }
        }

        fun getCardsForLot(): List<GameCard> {
            return listOf(
                GameCard.Factory.create(Suit.CLUB, GameCard.CardName.SEVEN),
                GameCard.Factory.create(Suit.HEART, GameCard.CardName.SEVEN),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.SEVEN),
                GameCard.Factory.create(Suit.SPADE, GameCard.CardName.EIGHT)
            )
        }
    }
}