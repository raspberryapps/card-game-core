package io.raspberryapps.card_game.core.data.models.cards

import io.raspberryapps.card_game.core.data.models.cards.GameCard.CardName


fun List<GameCard>.filterBySuit(vararg suit: Suit): List<GameCard> =
    filter(bySuitPredicate(*suit))

fun bySuitPredicate(vararg types: Suit) = { p1: GameCard ->
    types.any { p1.suit == it }
}

fun List<GameCard>.filterByCardName(vararg name: CardName): List<GameCard> =
    filter(byCardNamePredicate(*name))

fun byCardNamePredicate(vararg types: CardName) = { p1: GameCard ->
    types.any { p1.name == it }
}

object CardNumberComparator : Comparator<GameCard> {
    override fun compare(a: GameCard, b: GameCard): Int {
        return b.number - a.number
    }
}

object CardIndexComparator : Comparator<GameCard> {
    override fun compare(a: GameCard, b: GameCard): Int {
        return a.index - b.index
    }
}

class CardSuitSortingComparator(private val suit: List<Suit>) : Comparator<GameCard> {

    override fun compare(a: GameCard, b: GameCard): Int {
        if (!a.isReal || !b.isReal) {
            return 0
        }
        val suitIndexL = suit.indexOf(a.suit)
        val suitIndexR = suit.indexOf(b.suit)

        val sortedNumberL = suitIndexL * GameCard.SUIT_CARD_LINE_CARDS_SIZE + a.number
        val sortedNumberR = suitIndexR * GameCard.SUIT_CARD_LINE_CARDS_SIZE + b.number

        return sortedNumberL - sortedNumberR
    }
}

fun MutableList<GameCard>.distributeCardsBottom(count: Int): List<GameCard> {
    val cardsToDeal = take(count).toList()
    removeAll(cardsToDeal)
    return cardsToDeal
}

fun MutableList<GameCard>.distributeCardsTop(count: Int): List<GameCard> {
    val cardsToDeal = takeLast(count).toList()
    removeAll(cardsToDeal)
    return cardsToDeal
}