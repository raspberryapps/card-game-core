package io.raspberryapps.card_game.core.data.models.cards


fun Suit.getIndex(): Int {
    return when (this) {
        Suit.DIAMOND -> 0
        Suit.SPADE -> 1
        Suit.HEART -> 2
        Suit.CLUB -> 3
    }
}

fun getSuit(index: Int): Suit {
    when (index) {
        0 -> return Suit.DIAMOND
        1 -> return Suit.SPADE
        2 -> return Suit.HEART
        3 -> return Suit.CLUB
    }
    throw IllegalStateException("Can't support this suit $index")
}