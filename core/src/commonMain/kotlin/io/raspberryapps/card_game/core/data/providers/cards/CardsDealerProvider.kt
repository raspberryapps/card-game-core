package io.raspberryapps.card_game.core.data.providers.cards

import games.raspberry.card_game.random.data.models.CardDeckRequestUserData
import io.raspberryapps.card_game.core.data.models.cards.GameCard

interface CardsDealerProvider {

    /**
     * Creates new card deck, NOT shuffled
     *
     * Indexes default
     * [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13] == [2, 3, 4, 5, 6, 7, 8, 9, 10, J, D, K, T]
     *
     * @param leftCardIndexForEachSuit indicates shift for cardNames from left
     * @param rightCardIndexForEachSuit indicates shift for cardNames from right
     * Example:
     * leftCardIndexForEachSuit: 5
     * rightCardIndexForEachSuit: 1
     * [7, 8, 9, 10, J, D, K]
     */
    fun createNewCardDeck(
        leftCardIndexForEachSuit: Int = 0,
        rightCardIndexForEachSuit: Int = 0,
    ): List<GameCard>

    /**
     * Creates new card deck, shuffled
     *
     * Indexes default
     * [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13] == [2, 3, 4, 5, 6, 7, 8, 9, 10, J, D, K, T]
     *
     * @param leftCardIndexForEachSuit indicates shift for cardNames from left
     * @param rightCardIndexForEachSuit indicates shift for cardNames from right
     * Example:
     * leftCardIndexForEachSuit: 5
     * rightCardIndexForEachSuit: 1
     * [7, 8, 9, 10, J, D, K]
     */
    suspend fun createNewShuffledCardDeck(
        leftCardIndexForEachSuit: Int = 0,
        rightCardIndexForEachSuit: Int = 0,
        approximateRoundCountForGame: Int,
        userData: CardDeckRequestUserData,
        provider: CardsRandomProvider = CardsRandomProvider.CUSTOM
    ): List<GameCard>

    fun getNRandomUniqueCards(
        count: Int,
        cards: List<GameCard>,
    ): List<GameCard>

    fun standardShuffledCards(
        cards: List<GameCard>,
        times: Int = 3,
    ): List<GameCard>

    fun realLifeShuffledCards(
        cards: List<GameCard>,
        times: Int = 20,
    ): List<GameCard>

    enum class CardsRandomProvider {
        CUSTOM, RANDOM_ORG, RANDOM_ORG_SIGNED
    }

    fun getCardsCount(leftCardIndexForEachSuit: Int, rightCardIndexForEachSuit: Int): Int
    fun getCardsRange(leftCardIndexForEachSuit: Int, rightCardIndexForEachSuit: Int): IntRange
}