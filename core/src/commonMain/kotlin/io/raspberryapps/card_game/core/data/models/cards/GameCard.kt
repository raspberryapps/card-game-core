package io.raspberryapps.card_game.core.data.models.cards

data class GameCard(val index: Int) {

    // TODO swap suits https://busycats.atlassian.net/browse/DEB-419
    val suit: Suit
        get() {
            return when {
                index in 0..SUIT_CARD_LINE_CARDS_SIZE -> Suit.DIAMOND
                index <= SUIT_CARD_LINE_CARDS_SIZE * 2 -> Suit.SPADE
                index <= SUIT_CARD_LINE_CARDS_SIZE * 3 -> Suit.HEART
                index <= SUIT_CARD_LINE_CARDS_SIZE * 4 -> Suit.CLUB
                else -> throw IllegalStateException("Invalid suitCard in Card.getValue() $index")
            }
        }

    val number: Int
        get() = when {
            isStub -> STUB_CONST
            else -> {
                index - SUIT_CARD_LINE_CARDS_SIZE * suit.getIndex()
            }
        }

    val name: CardName
        get() = when {
            isStub -> CardName.STUB
            else -> getName(number)
        }

    val isReal: Boolean
        get() = !isShirt && !isStub

    private val isShirt: Boolean
        get() = index >= SHIRT && index <= SHIRT + CARD_DECK_SIZE

    val isStub: Boolean
        get() = index >= STUB && index <= STUB + CARD_DECK_SIZE

    override fun toString(): String {
        if (isShirt) {
            return "GameCard(SHIRT)"
        }
        if (isStub) {
            return "GameCard(STUB, $index)"
        }
        return "GameCard($suit$name, $index)"
    }

    enum class CardName {
        ACE, KING, DAME, JACK, TEN, NINE, EIGHT, SEVEN, SIX, FIVE, FOUR, TREE, TWO, STUB;

        override fun toString(): String {
            return getShortName()
        }

        private fun getShortName(): String {
            return when (this) {
                ACE -> "A"
                KING -> "K"
                DAME -> "D"
                JACK -> "J"
                TEN -> "10"
                NINE -> "9"
                EIGHT -> "8"
                SEVEN -> "7"
                SIX -> "6"
                FIVE -> "5"
                FOUR -> "4"
                TREE -> "3"
                TWO -> "2"
                STUB -> "STUB"
            }
        }
    }

    interface Factory {
        companion object {

            fun create(suit: Suit, number: Int): GameCard {
                return GameCard(
                    getNumber(
                        getName(number),
                        suit
                    )
                )
            }

            fun create(suit: Suit, cardNumber: CardName): GameCard {
                return GameCard(
                    getNumber(
                        cardNumber,
                        suit
                    )
                )
            }

            fun create(index: Int): GameCard {
                return GameCard(index)
            }

            /**
             * Stub card
             */
            fun createUniqueStub(cards: List<GameCard>): GameCard {
                return GameCard(
                    cards
                        .filter { it.isStub }
                        .maxOfWithOrNull(CardIndexComparator) { it }
                        ?.let { it.index + 1 }
                        ?: STUB
                )
            }

            /**
             * Stub card
             *
             * max = 53(STUB) + 4(playerIndex) * 14 = 109
             */
            fun createUniqueStub(
                position: Int,
                playerIndex: Int,
                previousStubCardWithMaxIndex: GameCard? = null,
                reverse: Boolean = false
            ): GameCard {
                val previousGameCardPosition = previousStubCardWithMaxIndex
                    ?.takeIf { it.isStub }?.index
                    ?.getStubPositionFromIndex(playerIndex, reverse)
                    ?.let { it + 1 } ?: 0
                val index = if (reverse) {
                    STUB + playerIndex * (SUIT_CARD_LINE_CARDS_SIZE + 1) - position - previousGameCardPosition
                } else {
                    STUB + playerIndex * SUIT_CARD_LINE_CARDS_SIZE + position + previousGameCardPosition
                }
                return GameCard(index)
            }

            /**
             * Stub card
             */
            fun createStub(): GameCard {
                return GameCard(STUB)
            }

            fun createUniqueShirt(
                position: Int = 0
            ): GameCard {
                val index = SHIRT + position
                return GameCard(index)
            }

            fun createShirt(): GameCard {
                return GameCard(SHIRT)
            }
        }
    }

    companion object {
        const val STUB_CONST = -1
        const val SUIT_CARD_LINE_CARDS_SIZE = 13
        const val CARD_DECK_SIZE = SUIT_CARD_LINE_CARDS_SIZE * 4

        private const val SHIRT = 150
        private const val STUB = 53

        /**
         * Transforms stub index to card position in hand
         */
        fun Int.getStubPositionFromIndex(
            playerIndex: Int,
            reverse: Boolean = false
        ): Int {
            return if (reverse) {
                this + (STUB + playerIndex * (SUIT_CARD_LINE_CARDS_SIZE + 1))
            } else {
                this - (STUB + playerIndex * SUIT_CARD_LINE_CARDS_SIZE)
            }
        }

        private fun getNumber(cardNumber: CardName, suit: Suit): Int {
            return suit.getIndex() * SUIT_CARD_LINE_CARDS_SIZE + getCardNumber(
                cardNumber
            )
        }

        private fun getCardNumber(number: CardName): Int {
            return when (number) {
                CardName.TWO -> 0
                CardName.TREE -> 1
                CardName.FOUR -> 2
                CardName.FIVE -> 3
                CardName.SIX -> 4
                CardName.SEVEN -> 5
                CardName.EIGHT -> 6
                CardName.NINE -> 7
                CardName.TEN -> 8
                CardName.JACK -> 9
                CardName.DAME -> 10
                CardName.KING -> 11
                CardName.ACE -> 12
                else -> throw IllegalStateException("Invalid CardNumber $number")
            }
        }

        private fun getName(number: Int): CardName {
            return when (number) {
                0 -> CardName.TWO
                1 -> CardName.TREE
                2 -> CardName.FOUR
                3 -> CardName.FIVE
                4 -> CardName.SIX
                5 -> CardName.SEVEN
                6 -> CardName.EIGHT
                7 -> CardName.NINE
                8 -> CardName.TEN
                9 -> CardName.JACK
                10 -> CardName.DAME
                11 -> CardName.KING
                12 -> CardName.ACE
                else -> throw IllegalStateException("Invalid index name in Card.getName() $number")
            }
        }
    }
}