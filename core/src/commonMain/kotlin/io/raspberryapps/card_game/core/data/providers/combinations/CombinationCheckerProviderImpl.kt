package io.raspberryapps.card_game.core.data.providers.combinations

import io.raspberryapps.card_game.core.data.models.cards.CardIndexComparator
import io.raspberryapps.card_game.core.data.models.cards.CardNumberComparator
import io.raspberryapps.card_game.core.data.models.cards.GameCard
import io.raspberryapps.card_game.core.data.models.cards.Suit

class CombinationsCheckerProviderImpl : CombinationsCheckerProvider {

    override fun findCardsInRowCombinations(
        cards: List<GameCard>,
        supportedCombinations: Set<Int>,
        checkFromIndex: Int,
        includeCardNames: Set<GameCard.CardName>?,
        suit: Suit?
    ): List<List<GameCard>> {
        val sortedByIndexCards = cards.toMutableList()
            .apply { sortWith(CardIndexComparator) }
            .filter { it.isReal }
            .toList()

        val combinations = mutableListOf<List<GameCard>>()
        // check combinations
        val combinationCards = mutableListOf<GameCard>()

        for (i in sortedByIndexCards.indices) {
            val previousCard = combinationCards.lastOrNull()
            val currentCard = sortedByIndexCards[i]
            val isNextInRow = previousCard != null && currentCard.index == previousCard.index + 1 && currentCard.suit == previousCard.suit
            val passedCardNamesCheck = includeCardNames == null || includeCardNames.contains(currentCard.name)
            val passedSuitCheck = suit == null || currentCard.suit == suit
            if (isNextInRow && passedSuitCheck && passedCardNamesCheck) {
                combinationCards.add(currentCard)
            } else {
                if (combinationCards.size > checkFromIndex - 1) {
                    combinations.addAll(
                        createCombinationsFromCardsInRow(
                            cards = combinationCards.toMutableList(), supportedCombinations = supportedCombinations
                        )
                    )
                }
                combinationCards.clear()
                if (passedSuitCheck && passedCardNamesCheck) {
                    combinationCards.add(currentCard)
                }
            }
        }

        if (combinationCards.size > checkFromIndex - 1) {
            combinations.addAll(
                createCombinationsFromCardsInRow(
                    cards = combinationCards.toMutableList(), supportedCombinations = supportedCombinations
                )
            )
        }

        return combinations
    }

    override fun findSameCardsNumberCombinations(
        cards: List<GameCard>,
        cardsCount: Int,
        includeCardNames: Set<GameCard.CardName>?,
    ): List<List<GameCard>> {
        val sortedByNumberCards = cards.toMutableList()
            .apply { sortWith(CardIndexComparator) }
            .filter { it.isReal }
            .filter { includeCardNames == null || includeCardNames.contains(it.name) }
            .toList()

        val combinations = sortedByNumberCards
            .groupBy { it.number }
            .filter { it.value.size >= cardsCount }
            .values
        return combinations
            .map { it.sortedWith(CardIndexComparator) }
    }

    override fun createCombinationsFromCardsInRow(
        cards: List<GameCard>,
        supportedCombinations: Set<Int>,
    ): List<List<GameCard>> {
        val shortestCombinationSupport = supportedCombinations.minOf { it }
        val cardsToCheck = cards.toMutableList()
        val combinations = mutableListOf<List<GameCard>>()
        while (cardsToCheck.size >= shortestCombinationSupport) {
            val combinationCards = createCombination(cardsToCheck, supportedCombinations)
            combinations.add(combinationCards.toList())
            cardsToCheck.removeAll(combinationCards)
        }
        // sort by card number [CardIndexComparator]
        return combinations.sortedBy { it.first().index }
    }

    private fun createCombination(
        cards: List<GameCard>,
        supportedCombinations: Set<Int>,
    ): List<GameCard> {
        val size = cards.size
        // combination supported
        if (supportedCombinations.contains(size)) {
            return cards
        }
        val fromLongestCombinations = supportedCombinations.sorted().asReversed()
        val longestCombinationSupport = fromLongestCombinations.first { it <= size }
        return cards.takeLast(longestCombinationSupport)
    }
}