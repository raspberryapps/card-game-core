package io.raspberryapps.card_game.core.data.providers.cards

import games.raspberry.card_game.random.data.models.CardDeckRequestUserData
import games.raspberry.card_game.random.data.providers.RandomPoolProvider
import games.raspberry.card_game.random.utils.random.*
import games.raspberry.logger.PlatformRaspberryLogger
import games.raspberry.logger.RaspberryLogger
import io.raspberryapps.card_game.core.data.models.cards.GameCard
import io.raspberryapps.card_game.core.data.models.cards.Suit
import io.raspberryapps.card_game.core.data.providers.cards.CardsDealerProvider.CardsRandomProvider.*

class CardsDealerProviderImpl private constructor(
    private val random: CustomRandom,
    private val randomPoolProvider: RandomPoolProvider,
    private val logger: RaspberryLogger,
) : CardsDealerProvider {

    /**
     * Returns NOT shuffled card deck
     */
    override fun createNewCardDeck(
        leftCardIndexForEachSuit: Int,
        rightCardIndexForEachSuit: Int
    ): List<GameCard> {
        return Suit.entries
            .flatMap { suit ->
                getCardsRange(leftCardIndexForEachSuit, rightCardIndexForEachSuit)
                    .map { GameCard.Factory.create(suit, it) }
            }
    }

    override fun getCardsRange(
        leftCardIndexForEachSuit: Int,
        rightCardIndexForEachSuit: Int
    ): IntRange {
        return (leftCardIndexForEachSuit until GameCard.SUIT_CARD_LINE_CARDS_SIZE - rightCardIndexForEachSuit)
    }

    override fun getCardsCount(
        leftCardIndexForEachSuit: Int,
        rightCardIndexForEachSuit: Int
    ): Int {
        return Suit.entries
            .sumOf {
                getCardsRange(leftCardIndexForEachSuit, rightCardIndexForEachSuit)
                    .count()
            }
    }

    override suspend fun createNewShuffledCardDeck(
        leftCardIndexForEachSuit: Int,
        rightCardIndexForEachSuit: Int,
        approximateRoundCountForGame: Int,
        userData: CardDeckRequestUserData,
        provider: CardsDealerProvider.CardsRandomProvider
    ): List<GameCard> {
        return when (provider) {
            CUSTOM -> createNewCardDeck(
                leftCardIndexForEachSuit = leftCardIndexForEachSuit,
                rightCardIndexForEachSuit = rightCardIndexForEachSuit,
            ).let { standardShuffledCards(it) }

            RANDOM_ORG -> getRandomOrgCards(
                leftCardIndexForEachSuit = leftCardIndexForEachSuit,
                rightCardIndexForEachSuit = rightCardIndexForEachSuit,
                approximateRoundsCount = approximateRoundCountForGame,
                userData = userData,
                signed = false,
            )

            RANDOM_ORG_SIGNED -> getRandomOrgCards(
                leftCardIndexForEachSuit = leftCardIndexForEachSuit,
                rightCardIndexForEachSuit = rightCardIndexForEachSuit,
                approximateRoundsCount = approximateRoundCountForGame,
                userData = userData,
                signed = true,
            )
        }
    }

    private suspend fun getRandomOrgCards(
        leftCardIndexForEachSuit: Int,
        rightCardIndexForEachSuit: Int,
        approximateRoundsCount: Int,
        userData: CardDeckRequestUserData,
        signed: Boolean,
    ): List<GameCard> {
        val cardsCount = getCardsCount(leftCardIndexForEachSuit, rightCardIndexForEachSuit)
        val result = randomPoolProvider.getCardDeck(
            userData = userData,
            cardsCount = cardsCount,
            min = 0,
            max = cardsCount - 1,
            minRoundsCountInPool = approximateRoundsCount,
            signed = signed,
        )

        return if (result.isSuccess) {
            normalizeCards(
                leftCardIndexForEachSuit = leftCardIndexForEachSuit,
                cards = result.getOrThrow().cards
            )
                .map { GameCard.Factory.create(it) }
        } else {
            val exception = result.exceptionOrNull()
            logger.v(
                tag = "CardsDealerProvider",
                message = "getRandomOrgCards: ${exception?.message}",
                exception = exception
            )
            createNewShuffledCardDeck(
                leftCardIndexForEachSuit = leftCardIndexForEachSuit,
                rightCardIndexForEachSuit = rightCardIndexForEachSuit,
                approximateRoundCountForGame = approximateRoundsCount,
                userData = userData,
                provider = CardsDealerProvider.CardsRandomProvider.CUSTOM,
            )
        }
    }

    /**
     * To reduce API usage we ask for card that we need only not the whole card deck,
     * because of that card indexes are shifted
     */
    private fun normalizeCards(
        cards: List<Int>,
        leftCardIndexForEachSuit: Int,
    ): List<Int> {
        val oneSuitCount = cards.size / Suit.entries.size
        val newIndexes = cards.map { index ->
            val previousSuitNumber = index / oneSuitCount
            val cardIndexShift = leftCardIndexForEachSuit * (previousSuitNumber + 1)
            val newIndex = index + cardIndexShift
            newIndex
        }
        return newIndexes
    }

    override fun getNRandomUniqueCards(count: Int, cards: List<GameCard>): List<GameCard> {
        return mutableListOf<GameCard>()
            .apply {
                for (card in cards) {
                    if (size == count) {
                        return@apply
                    }
                    val hasSimilarCard = contains(card)
                    if (hasSimilarCard) {
                        continue
                    }
                    if (any { it.number == card.number }) {
                        continue
                    }
                    add(card)
                }
            }
    }

    override fun standardShuffledCards(
        cards: List<GameCard>,
        times: Int,
    ): List<GameCard> {
        return cards.toMutableList().let {
            var shuffled = it
            for (i in 0 until times) {
                shuffled = random.shuffle(shuffled)
            }
            shuffled
        }
    }

    override fun realLifeShuffledCards(
        cards: List<GameCard>,
        times: Int,
    ): List<GameCard> {
        return cards.toMutableList()
            .riffle(times, random)
            .overhand(times, random)
            .toList()
    }

    companion object {
        fun withTimeSeed(
            randomPoolProvider: RandomPoolProvider,
            logger: RaspberryLogger = PlatformRaspberryLogger()
        ): CardsDealerProvider {
            return CardsDealerProviderImpl(
                random = DefaultRandom(),
                logger = logger,
                randomPoolProvider = randomPoolProvider
            )
        }

        fun withSecure(
            randomPoolProvider: RandomPoolProvider,
            logger: RaspberryLogger = PlatformRaspberryLogger()
        ): CardsDealerProvider {
            return CardsDealerProviderImpl(
                random = SecureRandom(logger),
                logger = logger,
                randomPoolProvider = randomPoolProvider
            )
        }
    }
}

