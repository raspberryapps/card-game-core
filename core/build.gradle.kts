import org.jetbrains.kotlin.gradle.plugin.mpp.apple.XCFramework
import com.codingfeline.buildkonfig.compiler.FieldSpec.Type.STRING

plugins {
    alias(libs.plugins.kotlinMultiplatform)
    alias(libs.plugins.kotlinSerialization)
    alias(libs.plugins.kotlinDokka)
    alias(libs.plugins.buildKonfig)
    alias(libs.plugins.ktLint)
    id("module.publication")
}

kotlin {
    jvm()
    js(IR) {
        binaries.library()
        nodejs()
        browser()
        generateTypeScriptDefinitions()
    }
    val xcf = XCFramework("core")
    listOf(
        iosX64(), iosArm64(), iosSimulatorArm64(), macosX64(), macosArm64(),
    ).forEach {
        it.binaries.framework {
            baseName = "core"
            xcf.add(this)
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                // Serialization
                implementation(libs.kotlinx.serialization.json)
                implementation(libs.kotlinx.datetime)
                implementation(libs.kotlinx.coroutines)
                implementation(libs.kotlinx.atomic)
                implementation(libs.secure.random)

                implementation(project(":random"))
                implementation(project(":logger"))
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(libs.kotlin.test)
                implementation(libs.kotlinx.coroutines.test)
            }
        }
    }
}

buildkonfig {
    packageName = "io.raspberryapps.cardgame.core"
    objectName = "CoreConfig"
    exposeObjectWithName = "CoreConfig"

    // default config is required
    defaultConfigs {
        buildConfigField(STRING, "version", "$version")
    }
}

ktlint {
    verbose.set(true)
    outputToConsole.set(true)
}
